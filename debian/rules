#!/usr/bin/make -f

#export DH_VERBOSE=1
#export PYBUILD_VERBOSE=1

# lintian says:
# This is needed (together with "relro") to make the "Global Offset Table"
# (GOT) fully read-only. The bindnow feature trades startup time for
# improved security. Please consider enabling this feature ...
export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
else
    NUMJOBS = True
endif
PICKLE_PROTOCOL=4
export PYBIK_NO_SETUPTOOLS=1

# pybik has to follow qt, and qtbase-opensource-src does this to select the gl variant:
gles2_architectures := armel armhf
ifeq ($(DEB_HOST_ARCH),$(findstring $(DEB_HOST_ARCH), $(gles2_architectures)))
	GLVARIANT = es2
else
	GLVARIANT = ogl
endif

%:
	dh $@ --buildsystem=pybuild

override_dh_auto_clean:
	dh_auto_clean
	rm -f debian/README
	rm -rf csrc

override_dh_auto_build-arch:
	dh_auto_build -- --build-args="--arch-only --variants=$(GLVARIANT)"

override_dh_auto_build-indep:
	python3 buildlib/create_docs.py --skip=Installation README=debian/README
	dh_auto_build -- --build-args=" \
	                        --indep-only \
	                        --pickle-protocol=$(PICKLE_PROTOCOL) \
	                        --reproducible"

override_dh_auto_install-arch:
	# build_ext don't know "--variants"-option and builds
	# the wrong extension modules on gles2_architectures without --skip-build
	dh_auto_install -- --install-args=" \
	                        --skip-build \
	                        --arch-only \
	                        --install-lib=/usr/lib/pybik"

override_dh_auto_install-indep:
	dh_auto_install -- --install-args=" \
	                        --indep-only \
	                        --install-lib=/usr/lib/pybik \
	                        --install-scripts=/usr/lib/pybik \
	                        --data-dir=/usr/share"

override_dh_auto_test:
	# test whether pickled data can be loaded with all supported Python versions
	PYBUILD_SYSTEM=custom \
	PYBUILD_TEST_ARGS="{interpreter} -c 'import glob,pickle; \
	           [(print(\"test with {version}:\", f), pickle.load(open(f, \"rb\"))) \
	           for f in glob.glob(\"build/models/*\")]'"

override_dh_installchangelogs:
	dh_installchangelogs NEWS

override_dh_python3:
	dh_python3 -p pybik
	dh_python3 -p pybik-bin usr/lib/pybik/pybiklib/
