Source: pybik
Section: games
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: B. Clausius <barcc@gmx.de>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               cython3-legacy,
               python3-all-dev,
               libgl-dev, libglvnd-dev,
               libgles-dev, libglvnd-dev,
               qtbase5-dev,
               qtbase5-dev-tools,
Build-Depends-Indep:
               gettext,
               python3-docutils,
Standards-Version: 4.6.2
Homepage: https://launchpad.net/pybik/
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/pybik.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pybik

Package: pybik
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends},
         pybik-bin (>= ${source:Version}),
         pybik-bin (<< ${source:Version}.1~),
Recommends: python3-icu,
            qttranslations5-l10n,
Description: Rubik's cube game
 Pybik is a 3D puzzle game about the cube invented by Ernő Rubik.
 .
  * Various 3D puzzles, such as cubes, towers, prisms, tetrahedra, octahedra,
  dodecahedra and many more
  * Solvers for some puzzles
  * Pretty patterns
  * Editor for move sequences
  * Custom colors and images on the surface of the puzzles

Package: pybik-bin
Architecture: any
Depends: ${python3:Depends}, ${shlibs:Depends}, ${misc:Depends},
Recommends: pybik
Replaces: pybik (<< ${source:Version})
Breaks: pybik (<< ${source:Version})
Multi-Arch: same
Description: Rubik's cube game - architecture dependent files
 This package contains the architecture dependent files of Pybik,
 and is generally useless without the pybik package.
