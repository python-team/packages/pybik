#version 120
//  Copyright © 2014-2015, 2017  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

//#extension GL_OES_standard_derivatives : enable

#ifdef GL_ES
precision mediump float;
#endif

varying vec3 barycentric;

//float edgeFactor()
//{
//    vec3 d = fwidth(barycentric);
//    vec3 a3 = smoothstep(vec3(0.0), d*1.5, barycentric);
//    return min(min(a3.x, a3.y), a3.z);
//}

void main()
{
    if (barycentric == vec3(0.))
        discard;
    else if (any(lessThan(barycentric, vec3(0.04))))
        gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    else
        gl_FragColor = vec4(0.5, 0.5, 0.5, 1.0);
    //else
    //    gl_FragColor = vec4(mix(vec3(1.0, 0.0, 0.0), vec3(0.5), edgeFactor()), 1.);
}

