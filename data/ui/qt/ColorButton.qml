//  Copyright © 2015  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2

Button {
    property alias color: colorDialog.color
    property alias currentColor: colorDialog.currentColor
    signal accepted()
    
    implicitWidth: 100
    Rectangle {
        anchors.centerIn: parent
        height: parent.height - 10
        width: parent.width - 30
        color: colorDialog.color
        border.width : 1
        border.color : "black"
    }
    ColorDialog {
        id: colorDialog
        modality: Qt.WindowModal
        onAccepted: color = currentColor
    }
    onClicked: colorDialog.open()
}

