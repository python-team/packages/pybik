//  Copyright © 2015-2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3

StackView {
    anchors.fill: parent
    SystemPalette { id: palette }
    
    function push_page(pagevisible, filename) {
        if (pagevisible)
            push(Qt.resolvedUrl(filename))
    }
    
    Component.onCompleted: {
        push(Qt.resolvedUrl("GamePage.qml"))
        push_page(ctx.model_pagevisible(), "ModelPage.qml")
        if (ctx.singlewin()) {
            push_page(ctx.preferences_visible, "PreferencesPage.qml")
            push_page(ctx.helpdialog_visible, "HelpPage.qml")
        }
        push_page(ctx.aboutdialog_visible, "AboutDialog.qml")
    }
    onCurrentItemChanged: if (currentItem) currentItem.focus = true
    
    Connections {
        target: ctx.singlewin() ? ctx : null
        onPreferences_visibleChanged: push_page(ctx.preferences_visible, "PreferencesPage.qml")
        onHelpdialog_visibleChanged: push_page(ctx.helpdialog_visible, "HelpPage.qml")
    }
    Connections {
        target: ctx
        onAboutdialog_visibleChanged: push_page(ctx.aboutdialog_visible, "AboutDialog.qml")
    }
    
    Loader {
        source: "PreferencesDialog.qml"
        active:  !ctx.singlewin() && ctx.preferences_visible
        onStatusChanged: { if (status==Loader.Ready) item.visible = true }
    }
    Loader {
        source: "HelpDialog.qml"
        active: !ctx.singlewin() && ctx.helpdialog_visible
        onStatusChanged: { if (status==Loader.Ready) item.visible = true }
    }
}


