//  Copyright © 2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3

ToolButton {
    id: root
    tooltip: ctx.tr_tooltip("Menu")
    checkable: true
    iconName: "format-justify-fill" //XXX: hmm
    property var model
    
    Rectangle {
        anchors.top: root.bottom
        anchors.right: root.right
        visible: root.checked
        border.width: 1
        implicitHeight: column.implicitHeight + 2
        width: column.width + 2
        
        Column {
            id: column
            x: 1
            y: 1
            z: 2
            width: repeater.width + ctx.gu(6)
            Repeater {
                id: repeater
                model: root.model
                
                Rectangle {
                    width: column.width
                    height: label.height + ctx.gu(6)
                    color: mousearea.containsMouse ? palette.highlight : palette.button
                    Label {
                        id: label
                        Component.onCompleted: repeater.width = Math.max(width, repeater.width)
                        x: ctx.gu(3)
                        y: ctx.gu(3)
                        text: modelData.text
                        color: mousearea.containsMouse ? palette.highlightedText : palette.buttonText
                    }
                    MouseArea {
                        id: mousearea
                        anchors.fill: parent
                        hoverEnabled: true
                        onClicked: { modelData.trigger(); root.checked = false }
                    }
                }
            }
        }
        MouseArea {
            visible: root.checked
            property var rootitem: ctx.rootitem()
            property var pos: Qt.point(0,0)
            x: -pos.x
            y: -pos.y
            width: rootitem.width
            height: rootitem.height
            onVisibleChanged: if (visible && rootitem) pos = parent.mapToItem(rootitem, 0, 0)
                                                       // == ctx.absolute_pos(parent, 0, 0)
            onClicked: root.checked = false
            //Rectangle { anchors.fill: parent; color: '#7700ff00' }
        }
    }
}

