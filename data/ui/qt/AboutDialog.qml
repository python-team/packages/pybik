//  Copyright © 2015-2017  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

FocusScope {
    id: root
    
    Item {
        anchors.fill: parent
        
        Rectangle {
            id: header
            width: parent.width
            height: headerrow.implicitHeight
            color: palette.window
            
            RowLayout {
                id: headerrow
                anchors.fill: parent
                
                ToolButton {
                    id: backbutton
                    iconName: "back"
                    onClicked: { ctx.aboutdialog_visible = false; root.Stack.view.pop() }
                }
                Image {
                    id: appicon
                    fillMode: Image.Pad
                    source: Qt.resolvedUrl(ctx.config("APPICON_FILE"))
                }
                Column {
                    id: col_apptext
                    Layout.fillWidth: true
                    spacing: 10
                    
                    Row {
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: ctx.gu(20)
                        
                        Label {
                            id: appname
                            font.pointSize: ctx.gu(16)
                            font.weight: Font.DemiBold
                            font.bold: true
                            text: ctx.config_tr("APPNAME")
                            textFormat: Text.PlainText
                        }
                        Label {
                            id: version
                            anchors.baseline: appname.baseline
                            text: ctx.config("VERSION")
                            textFormat: Text.PlainText
                    }   }
                    Label {
                        id: description
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: ctx.config_tr("SHORT_DESCRIPTION")
                        textFormat: Text.PlainText
                        wrapMode: Text.WordWrap
        }   }   }   }
        Rectangle {
            id: body
            anchors.top: header.bottom
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            implicitWidth: tabview.implicitWidth
            implicitHeight: tabview.implicitHeight
            color: palette.window
            
            TabView {
                id: tabview
                anchors.fill: parent
                
                Tab {
                    id: tab_about
                    title: ctx.tr("About")
                    property bool anim_paused: false
                    onVisibleChanged: anim_paused = false
                    
                    Item {
                        anchors.fill: parent
                        Column {
                            id: labels_about
                            anchors.margins: ctx.gu(6)
                            anchors.top: parent.top
                            anchors.left: parent.left
                            anchors.right: parent.right
                            spacing: 6
                            
                            Label {
                                textFormat: Text.PlainText
                                text: ctx.config("COPYRIGHT")
                            }
                            Label {
                                text: ctx.dialog_text("about_website")
                                textFormat: Text.StyledText
                                MouseArea {
                                    anchors.fill: parent
                                    acceptedButtons: Qt.NoButton
                                    cursorShape: Qt.PointingHandCursor
                                }
                                onLinkActivated: Qt.openUrlExternally(link)
                            }
                            Label {
                                text: ctx.tr("Translators:")
                                textFormat: Text.PlainText
                        }   }
                        TextArea {
                            id: translators
                            width: parent.width
                            anchors.top: labels_about.bottom
                            anchors.bottom: parent.bottom
                            text: ctx.dialog_text("about_translators")
                            textFormat: Text.RichText
                            readOnly: true
                            verticalScrollBarPolicy: tab_about.anim_paused ?
                                                        Qt.ScrollBarAlwaysOn : Qt.ScrollBarAlwaysOff
                            property real anim_from: -50.0
                            property real anim_to: 50.0 + contentHeight - viewport.height
                            property real anim_duration: (anim_to - anim_from) * 16.0 / ctx.gu(1)
                            
                            MouseArea {
                                anchors.fill: parent
                                enabled: !tab_about.anim_paused
                                cursorShape: enabled ? Qt.ArrowCursor: parent.hoveredLink=="" ?
                                                            Qt.IBeamCursor : Qt.PointingHandCursor
                                onPressed: { mouse.accepted = true; tab_about.anim_paused = true }
                                onWheel: { wheel.accepted = false; tab_about.anim_paused = true }
                            }
                            SequentialAnimation {
                                id: animation
                                running: root.visible
                                paused: !tab_about.visible || tab_about.anim_paused
                                loops: Animation.Infinite
                                
                                NumberAnimation {
                                    target: translators.contentItem
                                    property: "contentY"
                                    from: translators.anim_from
                                    to: translators.anim_to
                                    duration: Math.max(0, translators.anim_duration)
                                    easing.type: Easing.InOutSine
                                }
                                NumberAnimation {
                                    target: translators.contentItem
                                    property: "contentY"
                                    from: translators.anim_to
                                    to: translators.anim_from
                                    duration: Math.max(0, translators.anim_duration)
                                    easing.type: Easing.InOutSine
                            }   }
                            //Animation is updated only if stopped
                            onAnim_durationChanged: { if (animation.running) animation.restart() }
                            onLinkActivated: Qt.openUrlExternally(link)
                }   }   }
                Tab {
                    title: ctx.tr("Contribute")
                    
                    TextArea {
                        anchors.fill: parent
                        frameVisible: false
                        text: ctx.dialog_text("about_contribute")
                        readOnly: true
                        textFormat: Text.RichText
                        wrapMode: Text.WordWrap
                        
                        onLinkActivated: Qt.openUrlExternally(link)
                }   }
                Tab {
                    id: tab_license
                    title: ctx.tr("License")
                    property bool short_text: true
                    
                    TextArea {
                        anchors.fill: parent
                        frameVisible: false
                        function get_license_text(short_text) {
                            if (short_text) {
                                wrapMode = TextEdit.WordWrap
                                return ctx.dialog_text("about_license_short")
                            }
                            var longtext = ctx.dialog_text("about_license_full")
                            if (longtext) {
                                wrapMode = TextEdit.NoWrap
                                return longtext
                            }
                            wrapMode = TextEdit.WordWrap
                            return ctx.dialog_text("about_license_notfound")
                            
                        }
                        text: get_license_text(tab_license.short_text)
                        readOnly: true
                        textFormat: Text.RichText
                        wrapMode: TextEdit.WordWrap
                        
                        onLinkActivated: {
                            if (link.indexOf("text:")===0)
                                tab_license.short_text = false
                            else
                                Qt.openUrlExternally(link)
                        }
                    }
                    onVisibleChanged: { if (visible)  short_text = true }
                }
            }
        }
    }
    Keys.onPressed: {
        if (event.key == Qt.Key_Escape || event.key == Qt.Key_Return || event.key == Qt.Key_Enter) {
            //if (!event.accepted) {
                event.accepted = true
                ctx.aboutdialog_visible = false
                Stack.view.pop()
    }   }   //}
}

