//  Copyright © 2015-2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Window {
    id: root
    onVisibleChanged: { if (visible==false) ctx.preferences_visible = false }
    Component.onCompleted: ctx.set_transient(root)
    title: ctx.tr("Preferences")
    flags: Qt.Dialog
    modality: Qt.NonModal
    minimumWidth: verticalLayout.Layout.minimumWidth + 2*verticalLayout.anchors.margins
    minimumHeight: verticalLayout.Layout.minimumHeight + 2*verticalLayout.anchors.margins
    width: Math.max(200, minimumWidth)
    height: Math.max(200, minimumHeight)
    onMinimumWidthChanged: width = Math.max(width, minimumWidth)
    onMinimumHeightChanged: height = Math.max(height, minimumHeight)
    
    SystemPalette { id: palette }
    ListModel { id: facesmodel }
    
    ColumnLayout {
        id: verticalLayout
        anchors.margins: 5
        anchors.fill: parent
        focus: true
        
        PreferencesItem {}
        
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: 20
            Layout.minimumHeight: 0
            color: "transparent"
        }
        Button {
            Layout.alignment: Qt.AlignRight
            text: qsTranslate("QPlatformTheme", "Close")
            onClicked: root.close()
        }
        Keys.onPressed: {
            if (event.key == Qt.Key_Escape || event.key == Qt.Key_Return || event.key == Qt.Key_Enter) {
                if (!event.accepted) {
                    event.accepted = true
                    root.close()
        }   }   }
    } // ColumnLayout
}

