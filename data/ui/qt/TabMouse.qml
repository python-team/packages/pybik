//  Copyright © 2015-2017  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3
import QtQuick.Layouts 1.1

    GridLayout {
        anchors.fill: parent
        columns: 2
        
        ExclusiveGroup { id: mouseGroup }
        RadioButton {
            id: button_mousemode_quad
            //Layout.fillWidth: true
            //Layout.minimumWidth: implicitWidth
            exclusiveGroup: mouseGroup
            text: ctx.tr("Point and click, all directions")
            checked: ctx.get_settings("draw.selection") == 0
            onCheckedChanged: {if (checked) ctx.set_settings("draw.selection", 0)}
        }
        RadioButton {
            id: button_mousemode_ext
            //Layout.fillWidth: true
            ////Layout.minimumWidth: implicitWidth
            //Layout.minimumWidth: rbme_label.contentWidth
            exclusiveGroup: mouseGroup
            text: ctx.tr("Point and click, simplified")
            checked: ctx.get_settings("draw.selection") == 1
            onCheckedChanged: {if (checked) ctx.set_settings("draw.selection", 1)}
        }
        RadioButton {
            id: button_mousemode_gesture
            exclusiveGroup: mouseGroup
            text: ctx.tr("Gesture")
            checked: ctx.get_settings("draw.selection") == 2
            onCheckedChanged: {if (checked) ctx.set_settings("draw.selection", 2)}
        }
    }

