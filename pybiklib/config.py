#-*- coding:utf-8 -*-

#  Copyright © 2009, 2011-2017  B. Clausius <barcc@gmx.de>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


import os

N_ = lambda s: s
try:
    _
except NameError:
    _ = N_

AUTHOR = 'B. Clausius'
CONTACT_EMAIL = 'barcc@gmx.de'
CONTACT_WEB = 'https://answers.launchpad.net/pybik'
COPYRIGHT = 'Copyright © 2009-2017,  B. Clausius'
LICENSE_NAME = 'GPL-3.0+'
PACKAGE = 'pybik'
PACKAGE_FULL = 'pybik.barcc'
#TRANSLATORS: Name of the application, probably should not be translated.
APPNAME = N_('Pybik')
VERSION = '3.0'
VERSION_SHORT = VERSION.split('~')[0]
WEBSITE = 'https://launchpad.net/pybik/'
DOWNLOADS = 'https://launchpad.net/pybik/+download'
BUG_DATABASE = 'https://bugs.launchpad.net/pybik'
CONTACT_FILEBUG = BUG_DATABASE + '/+filebug'


# The following two lines are replaced by "setup.py install"
data_dir = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'data')
appdata_dir = data_dir
# The following line is replaced by "setup.py sdist"
RELEASE_DATE = '2017-10-22'

LOCALE_DIR   = os.path.join(data_dir, 'locale')
APPICON_FILE = os.path.join(appdata_dir, 'icons', 'pybik-64.png')
APPICON192_FILE = os.path.join(appdata_dir, 'icons', 'pybik-192.png')
LICENSE_FILE = os.path.join(appdata_dir, 'GPL-3')
MODELS_DIR   = os.path.join(appdata_dir, 'models')
PLUGINS_DIR  = os.path.join(appdata_dir, 'plugins')
UI_DIR       = os.path.join(appdata_dir, 'ui')
SHADER_DIR   = os.path.join(appdata_dir, 'shaders')

def get_config_home(*args):
    return os.path.join(os.environ.get('XDG_CONFIG_HOME', '') or os.path.expanduser("~/.config"), *args)
def get_data_home(*args):
    return os.path.join(os.environ.get('XDG_DATA_HOME', '') or os.path.expanduser("~/.local/share"), *args)
    
#TRANSLATORS: short description
SHORT_DESCRIPTION = N_("Rubik's cube game")
LONG_DESCRIPTION = (
    #TRANSLATORS: The next 7 lines belong together and form the long description
    N_('Pybik is a 3D puzzle game about the cube invented by Ernő Rubik.'),
    '',
    '* '+N_('Various 3D puzzles, such as cubes, towers, prisms, tetrahedra, octahedra, dodecahedra and many more'),
    '* '+N_('Solvers for some puzzles'),
    '* '+N_('Pretty patterns'),
    '* '+N_('Editor for move sequences'),
    '* '+N_('Custom colors and images on the surface of the puzzles'),
)

LICENSE_INFO = N_(
            'This program is free software: you can redistribute it and/or modify '
            'it under the terms of the GNU General Public License as published by '
            'the Free Software Foundation, either version 3 of the License, or '
            '(at your option) any later version.\n\n'

            'This program is distributed in the hope that it will be useful, '
            'but WITHOUT ANY WARRANTY; without even the implied warranty of '
            'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the '
            'GNU General Public License for more details.')
LICENSE_FURTHER = N_(
#TRANSLATORS: Text between "<" and ">" is expanded to a link by the program and should not be modified.
#             Text between "<LINK|>" and "<|>" is the translatable text for the link.
            'Read the full text of the <text:FULL_LICENSE_TEXT|>GNU General Public License'
            '<|> or see <http://www.gnu.org/licenses/>.')
LICENSE_NOT_FOUND = N_(
            'You should have received a copy of the GNU General Public License'
            ' along with this program.  If not, see <http://www.gnu.org/licenses/>.')

def get_filebug_text():
    return _(
    'If you find any bugs in Pybik or have a suggestion for an improvement then please '
    'submit a <{CONTACT_FILEBUG}|>bug report<|>.'
    ).format(CONTACT_FILEBUG=CONTACT_FILEBUG)

TRANSLATION_TEXT = N_(
    'Translations are managed by the '
    '<https://translations.launchpad.net/+groups/launchpad-translators|>'
    'Launchpad translation group<|>.\n\n'
    'If you want help to translate Pybik to your language you can do it through '
    'the <https://translations.launchpad.net/pybik|>web interface<|>.\n\n'
    'Read more about <https://help.launchpad.net/Translations|>"Translating with Launchpad"<|> '
    'and <https://help.launchpad.net/Translations/StartingToTranslate|>"Starting to translate"<|>.')

REPOSITORY_SHORT = 'lp:pybik'
REPOSITORY_URL = 'https://code.launchpad.net/~barcc/pybik/trunk'
REPOSITORY_BROWSE = 'https://bazaar.launchpad.net/~barcc/pybik/trunk/files'
REPOSITORY_DOWNLOAD = 'http://bazaar.launchpad.net/~barcc/pybik/trunk/download'


