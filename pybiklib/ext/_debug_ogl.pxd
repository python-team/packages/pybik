# generated from pybiklib/ext/gl_ogl.pxd

from libc.stddef cimport ptrdiff_t
from libc.stdint cimport int32_t, intptr_t, int8_t, uint8_t


# defines from /usr/include/GL/gl.h:

cdef enum:
    GL_FALSE = 				0
    GL_TRUE = 					1
    GL_UNSIGNED_BYTE = 			0x1401
    GL_FLOAT = 				0x1406
    GL_POINTS = 				0x0000
    GL_LINES = 				0x0001
    GL_TRIANGLES = 				0x0004
    GL_CCW = 					0x0901
    GL_BACK = 					0x0405
    GL_CULL_FACE = 				0x0B44
    GL_DEPTH_TEST = 				0x0B71
    GL_RGB = 					0x1907
    GL_RGBA = 					0x1908
    GL_MAX_TEXTURE_SIZE = 			0x0D33
    GL_TEXTURE_2D = 				0x0DE1
    GL_VENDOR = 				0x1F00
    GL_RENDERER = 				0x1F01
    GL_VERSION = 				0x1F02
    GL_EXTENSIONS = 				0x1F03
    GL_INVALID_ENUM = 				0x0500
    GL_INVALID_VALUE = 			0x0501
    GL_INVALID_OPERATION = 			0x0502
    GL_OUT_OF_MEMORY = 			0x0505
    GL_DEPTH_BUFFER_BIT = 			0x00000100
    GL_COLOR_BUFFER_BIT = 			0x00004000
    GL_MULTISAMPLE = 				0x809D
    GL_SAMPLE_ALPHA_TO_COVERAGE = 		0x809E
    GL_SAMPLE_COVERAGE = 			0x80A0
    GL_SAMPLE_BUFFERS = 			0x80A8
    GL_SAMPLES = 				0x80A9
    GL_SAMPLE_COVERAGE_VALUE = 		0x80AA
    GL_SAMPLE_COVERAGE_INVERT = 		0x80AB


# defines from /usr/include/GL/glext.h:

cdef enum:
    GL_ARRAY_BUFFER =                    0x8892
    GL_STATIC_DRAW =                     0x88E4
    GL_MAX_VERTEX_ATTRIBS =              0x8869
    GL_FRAGMENT_SHADER =                 0x8B30
    GL_VERTEX_SHADER =                   0x8B31
    GL_DELETE_STATUS =                   0x8B80
    GL_COMPILE_STATUS =                  0x8B81
    GL_LINK_STATUS =                     0x8B82
    GL_VALIDATE_STATUS =                 0x8B83
    GL_INFO_LOG_LENGTH =                 0x8B84
    GL_ATTACHED_SHADERS =                0x8B85
    GL_ACTIVE_UNIFORMS =                 0x8B86
    GL_ACTIVE_UNIFORM_MAX_LENGTH =       0x8B87
    GL_ACTIVE_ATTRIBUTES =               0x8B89
    GL_ACTIVE_ATTRIBUTE_MAX_LENGTH =     0x8B8A
    GL_SHADING_LANGUAGE_VERSION =        0x8B8C


# typedefs from /usr/include/GL/gl.h:

IF True:
    ctypedef unsigned int	GLenum
    ctypedef unsigned char	GLboolean
    ctypedef unsigned int	GLbitfield
    ctypedef void		GLvoid
    ctypedef int		GLint
    ctypedef unsigned char	GLubyte
    ctypedef unsigned int	GLuint
    ctypedef int		GLsizei
    ctypedef float		GLfloat
    ctypedef float		GLclampf


# typedefs from /usr/include/GL/glext.h:

IF True:
    ctypedef ptrdiff_t GLsizeiptr
    ctypedef ptrdiff_t GLintptr
    ctypedef char GLchar


# other typedefs:

ctypedef GLchar* const_GLchar_ptr


# functions from /usr/include/GL/gl.h:

IF True:
    cdef void  glClearColor( GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha ) nogil
    cdef void  glClear( GLbitfield mask ) nogil
    cdef void  glCullFace( GLenum mode ) nogil
    cdef void  glFrontFace( GLenum mode ) nogil
    cdef void  glEnable( GLenum cap ) nogil
    cdef void  glDisable( GLenum cap ) nogil
    cdef GLboolean  glIsEnabled( GLenum cap ) nogil
    cdef void  glGetBooleanv( GLenum pname, GLboolean *params ) nogil
    cdef void  glGetFloatv( GLenum pname, GLfloat *params ) nogil
    cdef void  glGetIntegerv( GLenum pname, GLint *params ) nogil
    cdef GLenum  glGetError() nogil
    cdef GLubyte *  glGetString( GLenum name ) nogil
    cdef void  glViewport( GLint x, GLint y,
                                    GLsizei width, GLsizei height ) nogil
    cdef void  glDrawArrays( GLenum mode, GLint first, GLsizei count ) nogil
    cdef void  glReadPixels( GLint x, GLint y,
                                    GLsizei width, GLsizei height,
                                    GLenum format, GLenum type,
                                    GLvoid *pixels ) nogil
    cdef void  glTexImage2D( GLenum target, GLint level,
                                    GLint internalFormat,
                                    GLsizei width, GLsizei height,
                                    GLint border, GLenum format, GLenum type,
                                    GLvoid *pixels ) nogil
    cdef void  glTexSubImage2D( GLenum target, GLint level,
                                       GLint xoffset, GLint yoffset,
                                       GLsizei width, GLsizei height,
                                       GLenum format, GLenum type,
                                       GLvoid *pixels ) nogil
    cdef void  glActiveTexture( GLenum texture ) nogil


# functions from /usr/include/GL/glext.h:

IF True:
    cdef void  glBindBuffer (GLenum target, GLuint buffer) nogil
    cdef void  glDeleteBuffers (GLsizei n, GLuint *buffers) nogil
    cdef void  glGenBuffers (GLsizei n, GLuint *buffers) nogil
    cdef void  glBufferData (GLenum target, GLsizeiptr size, void *data, GLenum usage) nogil
    cdef void  glBufferSubData (GLenum target, GLintptr offset, GLsizeiptr size, void *data) nogil
    cdef void  glAttachShader (GLuint program, GLuint shader) nogil
    cdef void  glBindAttribLocation (GLuint program, GLuint index, GLchar *name) nogil
    cdef void  glCompileShader (GLuint shader) nogil
    cdef GLuint  glCreateProgram () nogil
    cdef GLuint  glCreateShader (GLenum type) nogil
    cdef void  glDeleteProgram (GLuint program) nogil
    cdef void  glDeleteShader (GLuint shader) nogil
    cdef void  glDetachShader (GLuint program, GLuint shader) nogil
    cdef void  glDisableVertexAttribArray (GLuint index) nogil
    cdef void  glEnableVertexAttribArray (GLuint index) nogil
    cdef void  glGetActiveAttrib (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) nogil
    cdef void  glGetActiveUniform (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) nogil
    cdef GLint  glGetAttribLocation (GLuint program, GLchar *name) nogil
    cdef void  glGetProgramiv (GLuint program, GLenum pname, GLint *params) nogil
    cdef void  glGetProgramInfoLog (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog) nogil
    cdef void  glGetShaderiv (GLuint shader, GLenum pname, GLint *params) nogil
    cdef void  glGetShaderInfoLog (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) nogil
    cdef GLint  glGetUniformLocation (GLuint program, GLchar *name) nogil
    cdef void  glLinkProgram (GLuint program) nogil
    cdef void  glShaderSource (GLuint shader, GLsizei count, GLchar **string, GLint *length) nogil
    cdef void  glUseProgram (GLuint program) nogil
    cdef void  glUniform1i (GLint location, GLint v0) nogil
    cdef void  glUniformMatrix4fv (GLint location, GLsizei count, GLboolean transpose, GLfloat *value) nogil
    cdef void  glValidateProgram (GLuint program) nogil
    cdef void  glVertexAttribPointer (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, void *pointer) nogil

# GL version 2.0 needed

