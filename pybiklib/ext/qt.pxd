# -*- coding: utf-8 -*-

#  Copyright © 2015-2017  B. Clausius <barcc@gmx.de>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


from libc.stdint cimport int64_t
from libcpp cimport bool as cbool


ctypedef void CallbackVoid() nogil
ctypedef void CallbackInt(int) nogil
ctypedef void CallbackString(const QString &) nogil
ctypedef void CallbackDebugMessage(const QOpenGLDebugMessage &) nogil


#### QtCore ####

#global
cdef extern from "<QtCore/QtGlobal>":
    ctypedef unsigned char uchar
    ctypedef unsigned int uint
    ctypedef double qreal
    ctypedef int64_t qint64
    
#Qt
cdef extern from "<QtCore/Qt>" namespace "Qt":
    cdef enum FocusReason:
        OtherFocusReason
    cdef enum FocusPolicy:
        StrongFocus
    cdef enum ConnectionType:
        DirectConnection, QueuedConnection
    cdef enum KeyboardModifier:
        ShiftModifier, ControlModifier, KeypadModifier, AltModifier
    ctypedef int KeyboardModifiers
    cdef enum Key:
        Key_Period, Key_Escape, Key_Right, Key_Left, Key_Up, Key_Down,
        Key_Shift, Key_Control, Key_Meta, Key_Alt, Key_AltGr,
        Key_CapsLock, Key_NumLock, Key_ScrollLock
    cdef enum MouseButton:
        LeftButton, RightButton
    cdef enum CursorShape:
        CrossCursor
    cdef enum TransformationMode:
        FastTransformation, SmoothTransformation
    cdef enum AspectRatioMode:
        IgnoreAspectRatio
    cdef enum ImageConversionFlag:
        AutoColor
    cdef enum ApplicationAttribute:
        AA_X11InitThreads
        AA_UseDesktopOpenGL
        AA_UseSoftwareOpenGL
    cdef enum ItemDataRole:
        DisplayRole, UserRole
        
#QByteArray
cdef extern from "<QtCore/QByteArray>":
    cdef cppclass QByteArray:
        QByteArray() nogil
        QByteArray(const char *data) nogil
        const char *data() nogil const
        int size() nogil const
        
#QElapsedTimer
cdef extern from "<QtCore/QElapsedTimer>":
    cdef cppclass QElapsedTimer:
        QElapsedTimer() nogil
        void start() nogil
        cbool hasExpired(qint64 timeout) nogil const
        qint64 restart() nogil
        
#QEvent
cdef extern from "<QtCore/QEvent>":
    cdef cppclass QEvent:
        pass
        
#QFileSystemWatcher
cdef extern from "<QtCore/QFileSystemWatcher>":
    cdef cppclass QFileSystemWatcher:
        QFileSystemWatcher(QObject *parent)
        cbool addPath(const QString &file)
cdef extern from "<QtCore/QFileSystemWatcher>" namespace "QFileSystemWatcher":
    void directoryChanged(const QString &path) nogil
    void fileChanged(const QString &path) nogil
    
#QLibraryInfo
cdef extern from "<QtCore/QLibraryInfo>" namespace "QLibraryInfo":
    QString location(LibraryLocation loc)
    cdef enum LibraryLocation:
        TranslationsPath
        
#QList[QObject*]
cdef extern from "<QtCore/QList>":
    cdef cppclass QObjectList "QList<QObject*> ":
        #int size() nogil const
        #const QObject *at(int i) nogil const
        void reserve(int size) nogil
        void append(const QObject *t) nogil
        
#QList[QOpenGLDebugMessage]
cdef extern from "<QtCore/QList>":
    cdef cppclass OpenGLDebugMessageList "QList<QOpenGLDebugMessage>":
        int size() nogil const
        const QOpenGLDebugMessage& at(int i) nogil const
        
#QStringList
cdef extern from "<QtCore/QStringList>":
    cdef cppclass QStringList:
        int size() const
        const QString &operator[](int i) const
    
#QList[QVariant] == QVariantList
cdef extern from "<QtCore/QList>":
    cdef cppclass QVariantList "QList<QVariant> ":
        QVariantList()
        int size() const
        const QVariant& at(int i) const
        void reserve(int size)
        void append(const QVariant &t)
        
#QList[QUrl] == QUrlList
cdef extern from "<QtCore/QList>":
    cdef cppclass QUrlList "QList<QUrl> ":
        pass
        #QVariantList()
        #int size() const
        const QUrl& at(int i) const
        #void reserve(int size)
        #void append(const QUrl &t)
        
#QLocale
cdef extern from "<QtCore/QLocale>":
    cdef cppclass QLocale:
        QString name() const
cdef extern from "<QtCore/QLocale>" namespace "QLocale":
    QLocale QLocale_system "QLocale::system" ()
        
#QMap[QString,QVariant] == QVariantMap
cdef extern from "<QtCore/QMap>":
    cdef cppclass QVariantMap "QMap<QString, QVariant> ":
        QVariantMap()
        QStringList keys() const
        QVariantList values() const
        
#QMetaMethod
cdef extern from "<QtCore/QMetaMethod>":
    cdef cppclass QMetaMethod:
        QByteArray methodSignature() nogil const
        const char *typeName() nogil const
#QMetaObject
cdef extern from "<QtCore/QMetaObject>":
    cdef cppclass QMetaObject:
        const char *className() nogil const
        int propertyOffset() nogil const
        int propertyCount() nogil const
        QMetaProperty property(int index) nogil const
        int methodOffset() nogil const
        int methodCount() nogil const
        QMetaMethod method(int index) nogil const
#QMetaObject::Connection
cdef extern from "<QtCore/QMetaObject>" namespace "QMetaObject":
    cdef cppclass Connection:
        pass
    cbool invokeMethod(QObject *obj, const char *member, ConnectionType type) nogil
#QMetaProperty
cdef extern from "<QtCore/QMetaProperty>":
    cdef cppclass QMetaProperty:
        const char *name() nogil const
        const char *typeName() nogil const
        
#QMimeData
cdef extern from "<QtCore/QMimeData>":
    cdef cppclass QMimeData:
        QStringList formats() const
        cbool hasColor() const
        QVariant colorData() const
        cbool hasUrls() const
        QUrlList urls() const
        
#QModelIndex
cdef extern from "<QtCore/QModelIndex>":
    cdef cppclass QModelIndex:
        QVariant data(int role) const
        int row() const
        
#QObject
cdef extern from "<QtCore/QObject>":
    cdef cppclass QObject:
        QString objectName() nogil const
        const QMetaObject *metaObject() nogil const
        QVariant property(const char * name) nogil const
        cbool setProperty(const char *name, const QVariant &value) nogil
        cbool isWindowType() nogil const
cdef extern from "<QtCore/QObject>" namespace "QObject":
    Connection connect(const QTimer *sender, CallbackVoid *signal, CallbackVoid slot) nogil
    Connection connect(const QFileSystemWatcher *sender, CallbackString *signal, CallbackString slot) nogil
    Connection connect(const QApplication *sender, CallbackVoid *signal,
                       const QApplication *receiver, CallbackVoid *slot, ConnectionType type) nogil
    
#QPoint
cdef extern from "<QtCore/QPoint>":
    cdef cppclass QPoint:
        int x() nogil const
        int y() nogil const
        
#QPointF
cdef extern from "<QtCore/QPointF>":
    cdef cppclass QPointF:
        QPointF() nogil const
        qreal x() nogil const
        qreal y() nogil const
        void setX(qreal x) nogil
        void setY(qreal y) nogil
        
#QSize
cdef extern from "<QtCore/QSize>":
    cdef cppclass QSize:
        int	height() const
        int	width() const
        
#QStandardPaths
cdef extern from "<QtCore/QStandardPaths>" namespace "QStandardPaths":
    cdef enum StandardLocation:
        PicturesLocation, HomeLocation
    QStringList standardLocations(StandardLocation type)
    
#QString
cdef extern from "<QtCore/QString>":
    cdef cppclass QString:
        QString() nogil
        cbool isEmpty() nogil const
        QByteArray toUtf8() nogil const
cdef extern from "<QtCore/QString>" namespace "QString":
    QString fromUtf8(const char *str, int size) nogil
    QString QString_number "QString::number"(long n) nogil
    
#QTimer
cdef extern from "<QtCore/QTimer>":
    cdef cppclass QTimer:
        QTimer() nogil
        cbool isActive() nogil const
        void setInterval(int msec) nogil
        int interval() nogil const
        void setSingleShot(cbool singleShot) nogil
        void start(int msec) nogil
        void start() nogil
        void stop() nogil
        void deleteLater() nogil
cdef extern from "<QtCore/QTimer>" namespace "QTimer":
    void singleShot(int msec, CallbackVoid func) nogil
    void timeout() nogil
    
#QTranslator
cdef extern from "<QtCore/QTranslator>":
    cdef cppclass QTranslator:
        QTranslator()
        cbool load(const QString &filename, const QString &directory)
        
#QUrl
cdef extern from "<QtCore/QUrl>":
    cdef cppclass QUrl:
        QUrl()
        cbool isLocalFile() const
        QString toLocalFile() const
        QString toString() const
cdef extern from "<QtCore/QUrl>" namespace "QUrl":
    QUrl fromLocalFile(const QString &localfile)
    
#QVariant
cdef extern from "<QtCore/QVariant>":
    cdef cppclass QVariant:
        QVariant() nogil
        QVariant(int val) nogil
        cbool isValid() const
        cbool isNull() const
        void setValue[T](const T &value) nogil
        T value[T]() nogil const
        const char *typeName() nogil const
        int userType() const
        cbool canConvert[T]() nogil const
        QString toString() const
        
        
#### QtGui ####

#QCloseEvent
cdef extern from "<QtGui/QCloseEvent>":
    cdef cppclass QCloseEvent:
        pass
        
#QColor
cdef extern from "<QtGui/QColor>":
    cdef cppclass QColor:
        void setNamedColor(const QString& name)
        int red() const
        int green() const
        int blue() const
        qreal redF() const
        qreal greenF() const
        qreal blueF() const
        
#QCursor
cdef extern from "<QtGui/QCursor>":
    cdef cppclass QCursor:
        QCursor()
        QCursor(const QPixmap &pixmap, int hotX, int hotY)
        void setShape(CursorShape shape)
        
#QDragEnterEvent
cdef extern from "<QtGui/QDragEnterEvent>":
    cdef cppclass QDragEnterEvent:
        void acceptProposedAction()
        const QMimeData *mimeData() const
        
#QDropEvent
cdef extern from "<QtGui/QDropEvent>":
    cdef cppclass QDropEvent:
        void acceptProposedAction()
        const QMimeData *mimeData() const
        QPoint pos() const
        
#QFont
cdef extern from "<QtGui/QFont>":
    cdef cppclass QFont:
        QFont()
        void setFamily(const QString &family)
        void setPixelSize(int pixelSize)
        void setWeight(int weight)
        
#QIcon
cdef extern from "<QtGui/QIcon>":
    cdef cppclass QIcon:
        QIcon()
        QIcon(const QPixmap &pixmap)
        QIcon(const QString &fileName)
        void addFile(const QString &fileName)
        
#QImage
cdef extern from "<QtGui/QImage>" namespace "QImage":
    cdef enum Format:
        Format_RGBA8888
cdef extern from "<QtGui/QImage>":
    cdef cppclass QImage:
        QImage()
        QImage(const uchar *data, int width, int height, Format format)
        QImage(const QString &fileName)
        cbool load(const QString &fileName, const char* format)
        cbool save(const QString &fileName, const char *format, int quality) const
        cbool isNull() const
        int width() const
        int height() const
        QImage scaled(int w, int h, AspectRatioMode aspectMode, TransformationMode mode) const
        QImage mirrored(cbool horizontal, cbool vertical) const
        QImage transformed(const QTransform &matrix) const
        QImage convertToFormat(Format f, ImageConversionFlag flags) const
        const uchar *bits() const
        int byteCount() const
        
#QKeyEvent
cdef extern from "<QtGui/QKeyEvent>":
    cdef cppclass QKeyEvent:
        void ignore()
        KeyboardModifiers modifiers() const
        int key() const
        cbool matches(StandardKey key) const
        
#QKeySequence
cdef extern from "<QtGui/QKeySequence>":
    cdef cppclass QKeySequence:
        QKeySequence()
        QKeySequence(int k1, int k2, int k3, int k4)
        QKeySequence(const QString &key)
        int count() const
        int operator[](uint i) const
        QString toString(SequenceFormat format) const
cdef extern from "<QtGui/QKeySequence>" namespace "QKeySequence":
    cdef enum SequenceFormat:
        PortableText
    cdef enum StandardKey:
        MoveToNextWord, MoveToPreviousWord
    QKeySequence fromString(const QString &str, SequenceFormat format)
        
#QMouseEvent
cdef extern from "<QtGui/QMouseEvent>":
    cdef cppclass QMouseEvent:
        KeyboardModifiers modifiers() const
        MouseButton button() const
        int x() const
        int y() const
        
#QOpenGLContext
cdef extern from "<QtGui/QOpenGLContext>":
    cdef cppclass QOpenGLContext:
        QSurfaceFormat format() nogil const
        cbool isOpenGLES() nogil const
        cbool hasExtension(const QByteArray &extension) nogil const
        QOpenGLFunctions *functions() nogil const
cdef extern from "<QtGui/QOpenGLContext>" namespace "QOpenGLContext":
    QOpenGLContext *currentContext() nogil
    cdef enum OpenGLModuleType:
        LibGL, LibGLES
    OpenGLModuleType openGLModuleType() nogil
    
#QOpenGLDebugLogger
cdef extern from "<QtGui/QOpenGLDebugLogger>":
    cdef cppclass QOpenGLDebugLogger:
        QOpenGLDebugLogger(QObject *parent) nogil
        cbool initialize() nogil
        void enableMessages() nogil
        void startLogging(LoggingMode loggingMode) nogil
        OpenGLDebugMessageList loggedMessages() nogil const
cdef extern from "<QtGui/QOpenGLDebugLogger>" namespace "QOpenGLDebugLogger":
    void messageLogged(const QOpenGLDebugMessage &debugMessage) nogil
    cdef enum LoggingMode:
        AsynchronousLogging, SynchronousLogging
        
#QOpenGLDebugMessage
cdef extern from "<QtGui/QOpenGLDebugMessage>":
    ctypedef unsigned int GLuint
    cdef cppclass QOpenGLDebugMessage:
        GLuint id() nogil const
        QString message() nogil const
        #Severity severity() nogil const
        #Source source() nogil const
        
#QOpenGLFramebufferObjectFormat
cdef extern from "<QtGui/QOpenGLFramebufferObjectFormat>":
    cdef cppclass QOpenGLFramebufferObjectFormat:
        QOpenGLFramebufferObjectFormat() nogil
        int samples() nogil const
        void setSamples(int samples) nogil
        
#QOpenGLFramebufferObject
cdef extern from "<QtGui/QOpenGLFramebufferObject>":
    ctypedef unsigned int GLenum
    cdef cppclass QOpenGLFramebufferObject:
        QOpenGLFramebufferObject(int width, int height, GLenum target) nogil
        QOpenGLFramebufferObject(int width, int height, const QOpenGLFramebufferObjectFormat & format) nogil
        QOpenGLFramebufferObjectFormat format() nogil const
        void setAttachment(Attachment attachment) nogil
        cbool bind() nogil
        cbool release() nogil
        QImage toImage() nogil const
    cdef enum:
        GL_TEXTURE_2D
cdef extern from "<QtGui/QOpenGLFramebufferObject>" namespace "QOpenGLFramebufferObject":
    cdef enum Attachment:
        Depth
        
#QOpenGLFunctions
cdef extern from "<QtGui/QOpenGLFunctions>":
    cdef cppclass QOpenGLFunctions:
        OpenGLFeature openGLFeatures() nogil const
cdef extern from "<QtGui/QOpenGLFunctions>" namespace "QOpenGLFunctions":
    cdef enum OpenGLFeature:
        pass
        
#QOpenGLTexture
cdef extern from "<QtGui/QOpenGLTexture>":
    cdef cppclass QOpenGLTexture:
        QOpenGLTexture(Target target) nogil
        void setFormat(TextureFormat format) nogil
        void setSize(int width, int height, int depth) nogil
        void setMinMagFilters(Filter minificationFilter, Filter magnificationFilter) nogil
        void allocateStorage() nogil
        void bind() nogil
        void release() nogil
        void destroy() nogil
cdef extern from "<QtGui/QOpenGLTexture>" namespace "QOpenGLTexture":
    cdef enum Target:
        Target2D
    cdef enum TextureFormat:
        RGBA32F
    cdef enum Filter:
        Linear
        
#QPaintEvent
cdef extern from "<QtGui/QPaintEvent>":
    cdef cppclass QPaintEvent:
        pass
        
#QPixmap
cdef extern from "<QtGui/QPixmap>":
    cdef cppclass QPixmap:
        QPixmap()
        QPixmap(int width, int height)
        QPixmap(const QString & fileName)
        cbool convertFromImage(const QImage &image)
        cbool save(const QString &fileName, const char *format, int quality) const
cdef extern from "<QtGui/QPixmap>" namespace "QPixmap":
    QPixmap fromImage(const QImage &image)
    
#QResizeEvent
cdef extern from "<QtGui/QResizeEvent>":
    cdef cppclass QResizeEvent:
        const QSize &size() const
        
#QShowEvent
cdef extern from "<QtGui/QShowEvent>":
    cdef cppclass QShowEvent:
        cbool spontaneous() nogil const
        
#QStandardItem
cdef extern from "<QtGui/QStandardItem>":
    cdef cppclass QStandardItem:
        QStandardItem(const QString &text)
        void appendRow(QStandardItem *item)
        QVariant data(int role) const
        void setData(const QVariant &value)
        int row() const
        
#QStandardItemModel
cdef extern from "<QtGui/QStandardItemModel>":
    cdef cppclass QStandardItemModel:
        QStandardItemModel()
        QStandardItemModel(QObject *parent)
        void appendRow(QStandardItem *item)
        void clear()
        int rowCount(const QModelIndex &parent) const
        QModelIndex index(int row, int column, const QModelIndex &parent) const
        QStandardItem *item(int row, int column) const
        void setItem(int row, int column, QStandardItem *item)
        void setHorizontalHeaderItem(int column, QStandardItem *item)
        int rowCount() const
        
#QSurfaceFormat
cdef extern from "<QtGui/QSurfaceFormat>":
    cdef cppclass QSurfaceFormat:
        QSurfaceFormat() nogil
        int depthBufferSize() nogil const
        int stencilBufferSize() nogil const
        int redBufferSize() nogil const
        int greenBufferSize() nogil const
        int blueBufferSize() nogil const
        int alphaBufferSize() nogil const
        void setSamples(int numSamples) nogil
        int samples() nogil const
        SwapBehavior swapBehavior() nogil const
        OpenGLContextProfile profile() nogil const
        void setRenderableType(RenderableType type) nogil
        RenderableType renderableType() nogil const
        int majorVersion() nogil const
        int minorVersion() nogil const
        cbool stereo() nogil const
        void setSwapInterval(int interval) nogil
        int swapInterval() nogil const
        void setOption(FormatOption option) nogil
        FormatOption options() nogil const
cdef extern from "<QtGui/QSurfaceFormat>" namespace "QSurfaceFormat":
    cdef enum SwapBehavior:
        DefaultSwapBehavior, SingleBuffer, DoubleBuffer, TripleBuffer
    cdef enum RenderableType:
        DefaultRenderableType, OpenGL, OpenGLES, OpenVG
    cdef enum OpenGLContextProfile:
        NoProfile, CoreProfile, CompatibilityProfile
    cdef enum FormatOption:
        StereoBuffers, DebugContext
    void setDefaultFormat(const QSurfaceFormat &format) nogil
    QSurfaceFormat defaultFormat() nogil
    
#QTextDocumentFragment
cdef extern from "<QtGui/QTextDocumentFragment>":
    cdef cppclass QTextDocumentFragment:
        QString toHtml() const
cdef extern from "<QtGui/QTextDocumentFragment>" namespace "QTextDocumentFragment":
    QTextDocumentFragment fromPlainText(const QString &plainText)
        
#QTransform
cdef extern from "<QtGui/QTransform>":
    cdef cppclass QTransform:
        QTransform()
        QTransform &rotate(qreal angle)
        
#QWheelEvent
cdef extern from "<QtGui/QWheelEvent>":
    cdef cppclass QWheelEvent:
        QPoint angleDelta() const
        
        
#### QtWidgets ####

#QCoreApplication
cdef extern from "<QtCore/QCoreApplication>":
    cdef cppclass QCoreApplication:
        QStringList arguments()
        void setOrganizationName(const QString &orgName)
        void setApplicationName(const QString &application)
        void setApplicationVersion(const QString &version)
        cbool installTranslator(QTranslator *translationFile)
        int exec()
cdef extern from "<QtCore/QCoreApplication>" namespace "QCoreApplication":
    QCoreApplication *QCoreApplication_instance "QCoreApplication::instance"()
    
#QApplication
cdef extern from "<QtWidgets/QApplication>":
    cdef cppclass QApplication (QCoreApplication):
        QApplication(int &argc, char **argv)
        #QGuiApplication
        void setWindowIcon(const QIcon &icon)
        QFont font()
        void setFont(const QFont &font)
        qreal devicePixelRatio() const
cdef extern from "<QtWidgets/QApplication>" namespace "QApplication":
    void aboutToQuit() nogil
    void setAttribute(ApplicationAttribute attribute) nogil
    void processEvents() nogil
    #QGuiApplication
    void lastWindowClosed() nogil
    void sync() nogil
    
#QWidget
cdef extern from "<QtWidgets/QWidget>":
    cdef cppclass QWidget:
        void deleteLater() nogil
        QObject *sender() const
        QObject* findChild'findChild<QObject*>'(const QString &name) const
        void QWidget_showEvent"QWidget::showEvent"(QShowEvent *event) nogil
        void QWidget_closeEvent"QWidget::closeEvent"(QCloseEvent *event)
        void QWidget_keyPressEvent"QWidget::keyPressEvent"(QKeyEvent *event)
        void QWidget_resizeEvent"QWidget::resizeEvent"(QResizeEvent *event)
        cbool close()
        void setMinimumSize(int minw, int minh) nogil
        void setVisible(cbool visible)
        void resize(int w, int h)
        void show() nogil
        void update()
        void setTitle"setWindowTitle"(const QString &)
        void setWindowTitle(const QString &)
        void setWindowIcon(const QIcon &icon)
        void setFocus()
        QPixmap grab()
        
#QDialog
cdef extern from "<QtWidgets/QDialog>":
    cdef cppclass QDialog(QWidget):
        int exec()
        
#QFileDialog
cdef extern from "<QtWidgets/QFileDialog>" namespace "QFileDialog":
    QString getOpenFileName(QWidget *parent, const QString &caption, const QString &dir)
    
#QLabel
cdef extern from "<QtWidgets/QLabel>":
    cdef cppclass QLabel(QWidget):
        void setText(const QString &)
        
#QLineEdit
cdef extern from "<QtWidgets/QLineEdit>":
    cdef cppclass QLineEdit(QWidget):
        void QLineEdit_keyPressEvent'QLineEdit::keyPressEvent'(QKeyEvent *event)
        QString text() const
        void setText(const QString &)
        int cursorPosition() const
        void setCursorPosition(int)

#QListWidget
cdef extern from "<QtWidgets/QListWidget>":
    cdef cppclass QListWidget(QWidget):
        int	currentRow() const
        
#QListWidgetItem
cdef extern from "<QtWidgets/QListWidgetItem>":
    cdef cppclass QListWidgetItem:
        QListWidget *listWidget() const
        
#QMainWindow
cdef extern from "<QtWidgets/QMainWindow>":
    cdef cppclass QMainWindow(QWidget):
        QStatusBar *statusBar() const
        
#QMessageBox
cdef extern from "<QtWidgets/QMessageBox>":
    cdef cppclass QMessageBox(QWidget):
        QMessageBox(QWidget *parent) nogil
        void setText(const QString &text)
        void setIcon(int)
        void setStandardButtons(int buttons)
        int exec()
cdef extern from "<QtWidgets/QMessageBox>" namespace "QMessageBox":
    cdef enum:
        QMessageBox_Information 'QMessageBox::Information',
        QMessageBox_Close 'QMessageBox::Close',
        
#QOpenGLWidget
cdef extern from "<QtWidgets/QOpenGLWidget>":
    cdef cppclass QOpenGLWidget(QWidget):
        QOpenGLWidget(QWidget *parent) nogil
        int	height() nogil const
        void setAcceptDrops(cbool on) nogil
        void setFocusPolicy(FocusPolicy policy) nogil
        void setMouseTracking(cbool enable) nogil
        #QWidget *window() nogil const
        QSurfaceFormat format() nogil const
        void setSizePolicy(QSizePolicy) nogil
        void paintGL() nogil
        void setCursor(const QCursor &)
        void unsetCursor()
        void makeCurrent()
        void doneCurrent()
        QImage grabFramebuffer()
        
#QPushButton
cdef extern from "<QtWidgets/QPushButton>":
    cdef cppclass QPushButton:
        pass
        
#QSizePolicy
cdef extern from "<QtWidgets/QSizePolicy>":
    cdef cppclass QSizePolicy:
        QSizePolicy()
        QSizePolicy(Policy horizontal, Policy vertical)
        void setHorizontalStretch(int stretchFactor)
        void setVerticalStretch(int stretchFactor)
cdef extern from "<QtWidgets/QSizePolicy>" namespace "QSizePolicy":
    cdef enum Policy:
        Expanding
        
#QStatusBar
cdef extern from "<QtWidgets/QStatusBar>":
    cdef cppclass QStatusBar:
        void showMessage(const QString &message)
        void addWidget(QWidget *widget, int stretch)
        
#QTreeView
cdef extern from "<QtWidgets/QTreeView>":
    cdef cppclass QTreeView:
        pass
        
    
    

