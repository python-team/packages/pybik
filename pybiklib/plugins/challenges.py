#-*- coding:utf-8 -*-

#  Copyright © 2009-2013  B. Clausius <barcc@gmx.de>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


random = lambda game: game.random()
random1 = lambda game: game.random(1)
random2 = lambda game: game.random(2)
random3 = lambda game: game.random(3)
random4 = lambda game: game.random(4)
random5 = lambda game: game.random(5)
random6 = lambda game: game.random(6)
random7 = lambda game: game.random(7)
random8 = lambda game: game.random(8)
random9 = lambda game: game.random(9)
random10 = lambda game: game.random(10)

