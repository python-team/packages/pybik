#-*- coding:utf-8 -*-

#  Copyright © 2009-2015  B. Clausius <barcc@gmx.de>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

# pylint: disable=C0321


def play_stripes_Brick_nnn_2(game):
    size = game.current_state.model.sizes
    for unused_j in 0, 1:
        for i in range(size[1]-1, size[1]//2-1, -1):   game.add_moves([(1, i, 0)])
        for i in range(1, size[0], 2):                 game.add_moves([(0, i, 0)])
        for i in range(1, size[2], 2):                 game.add_moves([(2, i, 0)])
        
def play_stripes_Brick_nn1_4(game):
    size = game.current_state.model.sizes
    for i in range(1, size[0], 2):                 game.add_moves([(0, i, 0)])
    for i in range(size[1]-1, size[1]//2, -1):     game.add_moves([(1, i, 0)])
    for i in range(1, size[0] // 2, 2):            game.add_moves([(0, i, 0)])
    game.add_moves([(1, size[1]//2, 0)])
    for i in range(size[0]//2+1, size[0], 2):      game.add_moves([(0, i, 0)])
    for i in range(size[1]-1, size[1]//2-1, -1):   game.add_moves([(1, i, 0)])
    
def play_stripes_Brick_1nn_4(game):
    size = game.current_state.model.sizes
    for i in range(1, size[2], 2):                 game.add_moves([(2, i, 0)])
    for i in range(size[1]-1, size[1]//2, -1):     game.add_moves([(1, i, 0)])
    for i in range(1, size[2] // 2, 2):            game.add_moves([(2, i, 0)])
    game.add_moves([(1, size[1]//2, 0)])
    for i in range(size[2]//2+1, size[2], 2):      game.add_moves([(2, i, 0)])
    for i in range(size[1]-1, size[1]//2-1, -1):   game.add_moves([(1, i, 0)])
    
def play_stripes_Cube_nnn_2(game):
    size = game.current_state.model.sizes
    for i in range(0, size[0], 2): game.add_moves([(0, i, 0)] * 2)
    for i in range(0, size[2], 2): game.add_moves([(2, i, 0)] * 2)
    for i in range(0, size[0], 2): game.add_moves([(0, i, 0)] * 2)
    
def play_stripes_TowerBrick_nnn_2(game):
    size = game.current_state.model.sizes
    for i in range(0, size[0], 2): game.add_moves([(0, i, 0)])
    for i in range(0, size[2], 2): game.add_moves([(2, i, 0)])
    for i in range(0, size[0], 2): game.add_moves([(0, i, 0)])
    
def play_chessboard(game):
    model = game.current_state.model
    size = model.sizes
    dups = [s//2 for s in model.symmetries]
    
    # Moves are 3-Tuples (axis, slice, dir) axis=0,1,2 slice=0,...,size-1 dir=0,1
    if size[0] % 2 == size[1] % 2 == 1:
        moves =  [(0, i, 0) for i in range(1, size[0], 2)]
        moves += [(1, i, 0) for i in range(1, size[1], 2)]
        moves += [(2, i, 0) for i in range(1, size[2], 2)]
    elif size[1] % 2 == size[2] % 2 == 1:
        moves =  [(1, i, 0) for i in range(1, size[1], 2)]
        moves += [(2, i, 0) for i in range(1, size[2], 2)]
        moves += [(0, i, 0) for i in range(1, size[0], 2)]
    elif size[2] % 2 == size[0] % 2 == 1:
        moves =  [(2, i, 0) for i in range(1, size[2], 2)]
        moves += [(0, i, 0) for i in range(1, size[0], 2)]
        moves += [(1, i, 0) for i in range(1, size[1], 2)]
    for move in moves:
        game.add_moves([move] * dups[move[0]])
        
def play_t_time_Cube_odd(game):
    size = game.current_state.model.sizes
    ll = 'l{0}l{0}'.format(size[0] // 2 + 1)
    ff = 'f{0}f{0}'.format(size[0] // 2 + 1)
    game.add_code("{0}du-{1}ud-{0}duu{0}uu{0}d-{0}uu".format(ll, ff))
    
def play_t_time_Cube_even(game):
    size = game.current_state.model.sizes
    ll = 'l{0}l{0}l{1}l{1}'.format(size[0] // 2, size[0] // 2 + 1)
    ff = 'f{0}f{0}f{1}f{1}'.format(size[0] // 2, size[0] // 2 + 1)
    game.add_code("{0}dd2u-u2-{1}uu2d-d2-{0}dd2uuu2u2{0}uuu2u2{0}d-d2-{0}uuu2u2".format(ll, ff))
    
def play_t_time_Tower_odd(game):
    size = game.current_state.model.sizes
    ll = 'l{0}'.format(size[0] // 2 + 1)
    ff = 'f{0}'.format(size[0] // 2 + 1)
    game.add_code("{0}du-{1}ud-{0}duu{0}uu{0}d-{0}uu".format(ll, ff))
    
def play_t_time_Tower_even(game):
    size = game.current_state.model.sizes
    ll = 'l{0}l{1}'.format(size[0] // 2, size[0] // 2 + 1)
    ff = 'f{0}f{1}'.format(size[0] // 2, size[0] // 2 + 1)
    game.add_code("{0}dd2u-u2-{1}uu2d-d2-{0}dd2uuu2u2{0}uuu2u2{0}d-d2-{0}uuu2u2".format(ll, ff))
    
def play_plus_Cube_odd(game):
    size = game.current_state.model.sizes
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, size[1] - 1 - i, 0)] * 2)
    game.add_moves([(0, size[0] // 2, 0)] * 2)
    game.add_moves([(2, size[2] // 2, 0)] * 2)
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, i, 0)] * 2)
    game.add_moves([(0, size[0] // 2, 0)] * 2)
    game.add_moves([(2, size[2] // 2, 0)] * 2)
    
def play_plus_Cube_even(game):
    size = game.current_state.model.sizes
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, size[1] - 1 - i, 0)] * 2)
    game.add_moves([(0, size[0] // 2 - 1, 0)] * 2)
    game.add_moves([(0, size[0] // 2, 0)] * 2)
    game.add_moves([(2, size[2] // 2, 0)] * 2)
    game.add_moves([(2, size[2] // 2 - 1, 0)] * 2)
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, i, 0)] * 2)
    game.add_moves([(0, size[0] // 2 - 1, 0)] * 2)
    game.add_moves([(0, size[0] // 2, 0)] * 2)
    game.add_moves([(2, size[2] // 2, 0)] * 2)
    game.add_moves([(2, size[2] // 2 - 1, 0)] * 2)
    
def play_plus_Tower_on(game):
    size = game.current_state.model.sizes
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, size[1] - 1 - i, 0)] * 2)
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, i, 0)] * 2)
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    
def play_plus_Tower_en(game):
    size = game.current_state.model.sizes
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, size[1] - 1 - i, 0)] * 2)
    game.add_moves([(0, size[0] // 2 - 1, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    game.add_moves([(2, size[2] // 2 - 1, 0)])
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, i, 0)] * 2)
    game.add_moves([(0, size[0] // 2 - 1, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    game.add_moves([(2, size[2] // 2 - 1, 0)])
    
def play_plus_Brick_ono(game):
    size = game.current_state.model.sizes
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, size[1] - 1 - i, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, i, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    
def play_plus_Brick_one(game):
    size = game.current_state.model.sizes
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, size[1] - 1 - i, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    game.add_moves([(2, size[2] // 2 - 1, 0)])
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, i, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    game.add_moves([(2, size[2] // 2 - 1, 0)])
    
def play_plus_Brick_eno(game):
    size = game.current_state.model.sizes
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, size[1] - 1 - i, 0)])
    game.add_moves([(0, size[0] // 2 - 1, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, i, 0)])
    game.add_moves([(0, size[0] // 2 - 1, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    
def play_plus_Brick_ene(game):
    size = game.current_state.model.sizes
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, size[1] - 1 - i, 0)])
    game.add_moves([(0, size[0] // 2 - 1, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    game.add_moves([(2, size[2] // 2 - 1, 0)])
    for i in range(0, (size[1]-1) // 2): game.add_moves([(1, i, 0)])
    game.add_moves([(0, size[0] // 2 - 1, 0)])
    game.add_moves([(0, size[0] // 2, 0)])
    game.add_moves([(2, size[2] // 2, 0)])
    game.add_moves([(2, size[2] // 2 - 1, 0)])
    
def play_minus_CubeTower_odd(game):
    size = game.current_state.model.sizes
    game.add_moves([(1, size[1] // 2, 0)] * 2)
    
def play_minus_CubeTower_even(game):
    size = game.current_state.model.sizes
    game.add_moves([(1, size[1] // 2, 0)] * 2)
    game.add_moves([(1, size[1] // 2 - 1, 0)] * 2)
    
def play_minus_Brick_non(game):
    size = game.current_state.model.sizes
    game.add_moves([(1, size[1] // 2, 0)])
    
def play_minus_Brick_nen(game):
    size = game.current_state.model.sizes
    game.add_moves([(1, size[1] // 2, 0)])
    game.add_moves([(1, size[1] // 2 - 1, 0)])
    
    

