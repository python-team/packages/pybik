//generated from: build/temp.linux-x86_64-3.5/pybiklib/ext/qtui_.py

#include <QtCore/QString>    //line 31
QString gettext_translate(const char *text, const char *disambiguation);    //line 32
#include "../../data/ui/qt/main.h"    //line 33
#include "../../data/ui/qt/preferences.h"    //line 34
#include "../../data/ui/qt/help.h"    //line 35
#include "../../data/ui/qt/about.h"    //line 36
#include <QtCore/QPropertyAnimation>    //line 37
#include <QtGui/QDesktopServices>    //line 38
#include <QtGui/QKeyEvent>    //line 39
#include <QtGui/QStandardItemModel>    //line 40
#include <QtGui/QIconEngine>    //line 41
#include <QtGui/QPainter>    //line 42
#include <QtWidgets/QStyledItemDelegate>    //line 43
#include <QtWidgets/QWidget>    //line 44
#include <QtWidgets/QOpenGLWidget>    //line 45
#include <QtWidgets/QLineEdit>    //line 46
#include <QtWidgets/QColorDialog>    //line 47
#include <QtWidgets/QScrollBar>    //line 48
#include <QtWidgets/QListWidget>    //line 49
#include <QtWidgets/QListWidgetItem>    //line 50
#include <QtWidgets/QMenu>    //line 51

class ShortcutEditor : public QLabel    //line 89
{    //line 89
    Q_OBJECT    //line 89

public:    //line 89
    QString *key;    //line 91

    ShortcutEditor(QWidget *parent);    //line 94
    Q_SIGNAL void key_pressed();    //line 101

    void keyPressEvent(QKeyEvent *event);    //line 104
};    //line 124

class ShortcutDelegate : public QStyledItemDelegate    //line 126
{    //line 126
    Q_OBJECT    //line 126

public:    //line 126
    ShortcutDelegate(QObject *parent) : QStyledItemDelegate(parent) {}    //line 128

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;    //line 131
    void setEditorData(QWidget *editor, const QModelIndex &index) const {}    //line 138
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;    //line 141

    Q_INVOKABLE void on_editor_key_pressed();    //line 151
};    //line 157

class ColorIconEngine : public QIconEngine    //line 159
{    //line 159
public:    //line 159
    QString color;    //line 161

    ColorIconEngine() : QIconEngine(), color(QStringLiteral("black")) {}    //line 163

    void paint(QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state);    //line 166
    ColorIconEngine *clone() const;    //line 169
};    //line 175

class UIContainer    //line 178
{    //line 178
public:    //line 178
    Ui::MainWindow ui_main;    //line 179
    QLabel *label_selectmodel;    //line 180
    int active_plugin_group;    //line 181
    Ui::DialogPreferences ui_pref;    //line 182
    QStandardItemModel *liststore_movekeys;    //line 183
    QStandardItemModel *liststore_faces;    //line 184
    int liststore_faces_width;    //line 185
    ColorIconEngine *iconengine_color;    //line 186
    ColorIconEngine *iconengine_background_color;    //line 187
    Ui::DialogHelp ui_help;    //line 188
    Ui::AboutDialog ui_about;    //line 189
    int index_tab_about;    //line 190
    int index_tab_license;    //line 191
    QScrollBar *scrollbar;    //line 192
    QPropertyAnimation animation;    //line 193

    void setupUi_main(QMainWindow *window, const QIcon &icon1, const QIcon &icon2, const QIcon &icon3, const QKeySequence &key1, const QKeySequence &key2);    //line 196
    void disable_tooltips();    //line 316
    void set_shortcuts(const QKeySequence &key1, const QKeySequence &key2, const QKeySequence &key3, const QKeySequence &key4);    //line 329
    void add_widgets(QMainWindow *window, QLineEdit *edit, QOpenGLWidget *drawing);    //line 337
    void set_toolbar_visible(bool visible);    //line 350
    void set_frame_editbar_visible(bool visible);    //line 353
    void set_statusbar_visible(bool visible);    //line 356
    void hide_controls();    //line 359
    void set_visible(bool debug_text_visible, bool editbar_visible, bool statusbar_visible);    //line 369
    void get_splitter_sizes(int &s1, int &s2);    //line 379
    void set_splitter_sizes(int s1, int s2);    //line 386
    void splitter_update_minimumsize();    //line 394
    void set_debug_text(const QString &text);    //line 407
    void set_toolbar_state(int a, int b, int c, int d, int e);    //line 411
    void clear_sidepane();    //line 428
    QPushButton *create_sidepane_button(const QString &text);    //line 436
    QTreeView *create_sidepane_treeview(QStandardItemModel *treestore, int i);    //line 446
    void set_active_plugin_group(int i);    //line 461
    void set_active_plugin_group_by_obj(QObject *obj);    //line 474
    int get_plugin_group_count();    //line 480
    QModelIndex root_index(int i);    //line 483
    void hide_row(int i, bool hide);    //line 486
    void set_row_hidden(int i, int row, const QModelIndex &index, bool hide);    //line 497
    int splitter_pos();    //line 500
    // ####    //line 503
    void setupUi_pref(QDialog *window, int speed, int speed_min, int speed_max, int mirror_min, int mirror_max, QStandardItemModel *_movekeys);    //line 505
    void preferences_block_signals(bool block);    //line 559
    void combobox_add_shaderitem(const QString &name, const QString &nick);    //line 566
    void combobox_add_samplesitem(const QString &name, const QString &nick);    //line 569
    void add_movekey_row();    //line 572
    void remove_movekey_row();    //line 583
    void add_liststore_faces_row(const QString &name, const QString &key);    //line 589
    void finalize_liststore_faces(QWidget *window);    //line 600
    QString get_liststore_faces_facekey(const QModelIndex &index);    //line 611
    void set_button_color(const QString &color);    //line 614
    QString color_dialog(QWidget *parent, const QString &color);    //line 620
    void set_imagemode(int imagemode);    //line 630
    void add_combobox_image_item(const QIcon &icon, const QString &text, const QString &filename);    //line 638
    void set_combobox_image(int index_icon, const QIcon &icon, int index);    //line 641

    void set_button_background_color(const QString &color);    //line 648
    void fill_page(QVariant items, const QString &title);    //line 655
    void set_page(int i);    //line 670
    void setupUi_help(QDialog *window, const QString &helptext);    //line 678
    void setupUi_about(QDialog *window);    //line 685
    void fill_header(const QString &fileName, const QString &appname, const QString &version, const QString &desc);    //line 698
    void fill_about_tab(const QString &copyr, const QString &website, const QString &translators);    //line 706
    void fill_contribute_tab(const QString &contribute);    //line 713
    void fill_license_tab(const QString &license_short, bool license_notfound, const QString &license_full);    //line 716
    void tab_widget_currentChanged(int index);    //line 725
    void update_animation(bool first);    //line 740
    void event_filter_stop_animation(QEvent *event);    //line 760
    void show_full_license(bool local, const QUrl &url);    //line 769
};    //line 778

QString mk_qstr(const char *pstr, int length);    //line 782
