//generated from: build/temp.linux-x86_64-3.5/pybiklib/ext/qtui_.py

#include "_qtui__moc.h"    //line 53
#include "qtui_p.h"    //line 54

    ShortcutEditor::ShortcutEditor(QWidget *parent)    //line 94
        : QLabel(gettext_translate("Press a key …", Q_NULLPTR), parent)    //line 95
        {    //line 96
            setFocusPolicy(Qt::StrongFocus);    //line 97
            setAutoFillBackground(true);    //line 98
        }    //line 99

    void ShortcutEditor::keyPressEvent(QKeyEvent *event)    //line 104
        {    //line 105
            switch (event->key()) {    //line 106
                case Qt::Key_Shift: case Qt::Key_Control: case Qt::Key_Meta:    //line 107
                case Qt::Key_Alt: case Qt::Key_AltGr:    //line 108
                case Qt::Key_CapsLock: case Qt::Key_NumLock: case Qt::Key_ScrollLock:    //line 109
                    QLabel::keyPressEvent(event);    //line 110
                    return;    //line 111
            }    //line 112
            if (event->count() != 1) {    //line 113
                QLabel::keyPressEvent(event);    //line 114
                return;    //line 115
            }    //line 116
            int mod = event->modifiers() & (Qt::ShiftModifier | Qt::ControlModifier);    //line 117
            QStringList keylist = QKeySequence(event->key() | int(mod)).toString().split('+');    //line 118
            if ((event->modifiers() & Qt::KeypadModifier) && !keylist.contains(QStringLiteral("Num")))    //line 119
                keylist.insert(keylist.size()-1, "Num");    //line 120
            key = new QString(keylist.join('+'));    //line 121
            key_pressed();    //line 122
        }    //line 123
    QWidget *ShortcutDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const    //line 131
        {    //line 132
            ShortcutEditor *editor = new ShortcutEditor(parent);    //line 133
            connect(editor, SIGNAL(key_pressed()), this, SLOT(on_editor_key_pressed()));    //line 134
            return editor;    //line 135
        }    //line 136


    void ShortcutDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const    //line 141
        {    //line 142
            if (((ShortcutEditor*)editor)->key != NULL) {    //line 143
                QVariant value(*((ShortcutEditor*)editor)->key);    //line 144
                model->setData(index, value, Qt::DisplayRole);    //line 145
            }    //line 146
        }    //line 147

    Q_INVOKABLE void ShortcutDelegate::on_editor_key_pressed()    //line 151
        {    //line 152
            QWidget *editor = (QWidget*)this->sender();    //line 153
            this->commitData(editor);    //line 154
            this->closeEditor(editor, QAbstractItemDelegate::NoHint);    //line 155
        }    //line 156
    void ColorIconEngine::paint(QPainter *painter, const QRect &rect, QIcon::Mode mode, QIcon::State state)    //line 166
        { painter->fillRect(rect, QColor(this->color)); }    //line 167
    ColorIconEngine *ColorIconEngine::clone() const    //line 169
        {    //line 170
            ColorIconEngine *cloned = new ColorIconEngine();    //line 171
            cloned->color = color;    //line 172
            return cloned;    //line 173
        }    //line 174
    void UIContainer::setupUi_main(QMainWindow *window, const QIcon &icon1, const QIcon &icon2, const QIcon &icon3, const QKeySequence &key1, const QKeySequence &key2)    //line 196
        {    //line 197
            // actions that belongs to no widget    //line 198
            QAction *action_jump_to_editbar = new QAction(window);    //line 199
            action_jump_to_editbar->setObjectName("action_jump_to_editbar");    //line 200
            action_jump_to_editbar->setShortcut(key1);    //line 201
            window->addAction(action_jump_to_editbar);    //line 202
            QAction *action_edit_cube = new QAction(window);    //line 203
            action_edit_cube->setObjectName("action_edit_cube");    //line 204
            action_edit_cube->setShortcut(key2);    //line 205
            window->addAction(action_edit_cube);    //line 206
            // Qt Designer code    //line 208
            ui_main.setupUi(window);    //line 209
            // ...    //line 211
            ui_main.splitter->setCollapsible(0, false);    //line 212
            ui_main.splitter->setStretchFactor(0, 1);    //line 213
            ui_main.splitter->setStretchFactor(1, 0);    //line 214
            ui_main.listwidget->setItemDelegate(new ModelSelectionItemDelegate());    //line 215
            // set icons    //line 217
            ui_main.button_edit_exec->setIcon(QIcon::fromTheme("system-run"));    //line 218
            ui_main.button_edit_clear->setIcon(QIcon::fromTheme("edit-clear"));    //line 219
            ui_main.action_challenge->setIcon(icon1);    //line 220
            ui_main.action_new_solved->setIcon(icon2);    //line 221
            ui_main.action_quit->setIcon(QIcon::fromTheme("application-exit"));    //line 222
            ui_main.action_rewind->setIcon(QIcon::fromTheme("media-seek-backward"));    //line 223
            ui_main.action_previous->setIcon(QIcon::fromTheme("media-skip-backward"));    //line 224
            ui_main.action_stop->setIcon(QIcon::fromTheme("media-playback-stop"));    //line 225
            ui_main.action_play->setIcon(QIcon::fromTheme("media-playback-start"));    //line 226
            ui_main.action_next->setIcon(QIcon::fromTheme("media-skip-forward"));    //line 227
            ui_main.action_forward->setIcon(QIcon::fromTheme("media-seek-forward"));    //line 228
            ui_main.action_mark_set->setIcon(QIcon::fromTheme("list-add"));    //line 229
            ui_main.action_mark_remove->setIcon(QIcon::fromTheme("list-remove"));    //line 230
            ui_main.action_selectmodel->setIcon(icon3);    //line 231
            ui_main.action_selectmodel_back->setIcon(QIcon::fromTheme("back"));    //line 232
            ui_main.action_preferences->setIcon(QIcon::fromTheme("document-properties"));    //line 233
            ui_main.action_info->setIcon(QIcon::fromTheme("help-about"));    //line 234
            ui_main.action_help->setIcon(QIcon::fromTheme("help"));    //line 235
            // widgets that are not created with Qt Designer: toolbar_play    //line 237
            ui_main.toolbar_play->addAction(ui_main.action_selectmodel);    //line 238
            ui_main.toolbar_play->addAction(ui_main.action_new_solved);    //line 239
            ui_main.toolbar_play->addAction(ui_main.action_challenge);    //line 240
            ui_main.toolbar_play->addSeparator();    //line 241
            ui_main.toolbar_play->addAction(ui_main.action_rewind);    //line 242
            ui_main.toolbar_play->addAction(ui_main.action_previous);    //line 243
            ui_main.toolbar_play->addAction(ui_main.action_stop);    //line 244
            ui_main.toolbar_play->addAction(ui_main.action_play);    //line 245
            ui_main.toolbar_play->addAction(ui_main.action_next);    //line 246
            ui_main.toolbar_play->addAction(ui_main.action_forward);    //line 247
            ui_main.toolbar_play->addAction(ui_main.action_mark_set);    //line 248
            ui_main.toolbar_play->addAction(ui_main.action_mark_remove);    //line 249
            // …: toolbar_selectmodel    //line 251
            ui_main.toolbar_selectmodel->addAction(ui_main.action_selectmodel_back);    //line 252
            label_selectmodel = new QLabel();    //line 253
            ui_main.toolbar_selectmodel->addWidget(label_selectmodel);    //line 254
            ui_main.toolbar_selectmodel->setVisible(false);    //line 255
            // …: toolbar_common    //line 257
            QWidget *spacer = new QWidget();    //line 258
            spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);    //line 259
            ui_main.toolbar_common->addWidget(spacer);    //line 260
            // …: menu button    //line 262
            QToolButton *menubutton = new QToolButton(window);    //line 263
            menubutton->setIcon(QIcon::fromTheme("format-justify-fill")); //XXX: hmm    //line 264
            menubutton->setPopupMode(QToolButton::InstantPopup);    //line 265
            QMenu *menu = new QMenu(menubutton);    //line 266
            menu->addAction(ui_main.action_initial_state);    //line 267
            menu->addAction(ui_main.action_reset_rotation);    //line 268
            menu->addSeparator();    //line 269
            menu->addAction(ui_main.action_editbar);    //line 270
            menu->addAction(ui_main.action_statusbar);    //line 271
            menu->addSeparator();    //line 272
            menu->addAction(ui_main.action_preferences);    //line 273
            menu->addAction(ui_main.action_help);    //line 274
            menu->addAction(ui_main.action_info);    //line 275
            menu->addSeparator();    //line 276
            menu->addAction(ui_main.action_quit);    //line 277
            menubutton->setMenu(menu);    //line 278
            ui_main.toolbar_common->addWidget(menubutton);    //line 279
            // add action to main window for shortcuts    //line 281
            window->addAction(ui_main.action_challenge);    //line 282
            window->addAction(ui_main.action_new_solved);    //line 283
            window->addAction(ui_main.action_quit);    //line 284
            window->addAction(ui_main.action_selectmodel);    //line 285
            window->addAction(ui_main.action_initial_state);    //line 286
            window->addAction(ui_main.action_reset_rotation);    //line 287
            window->addAction(ui_main.action_preferences);    //line 288
            window->addAction(ui_main.action_statusbar);    //line 289
            window->addAction(ui_main.action_info);    //line 290
            window->addAction(ui_main.action_rewind);    //line 291
            window->addAction(ui_main.action_rewind);    //line 292
            window->addAction(ui_main.action_previous);    //line 293
            window->addAction(ui_main.action_previous);    //line 294
            window->addAction(ui_main.action_stop);    //line 295
            window->addAction(ui_main.action_stop);    //line 296
            window->addAction(ui_main.action_play);    //line 297
            window->addAction(ui_main.action_play);    //line 298
            window->addAction(ui_main.action_next);    //line 299
            window->addAction(ui_main.action_next);    //line 300
            window->addAction(ui_main.action_forward);    //line 301
            window->addAction(ui_main.action_forward);    //line 302
            window->addAction(ui_main.action_mark_set);    //line 303
            window->addAction(ui_main.action_mark_set);    //line 304
            window->addAction(ui_main.action_mark_remove);    //line 305
            window->addAction(ui_main.action_mark_remove);    //line 306
            window->addAction(ui_main.action_editbar);    //line 307
            window->addAction(ui_main.action_help);    //line 308
            window->addAction(ui_main.action_help);    //line 309
            // rest    //line 311
            disable_tooltips();    //line 312
            active_plugin_group = 0;    //line 313
        }    //line 314
    void UIContainer::disable_tooltips()    //line 316
        {    //line 317
            //pita: If a action has no tooltip, the appropriate toolbutton uses the actions text for the tooltip.    //line 318
            //      To remove tooltips you have to remove the tooltips for the appropriate toolbutton,    //line 319
            //      but every time you change something on the action, the button will recreate the tooltip    //line 320
            ui_main.toolbar_play->widgetForAction(ui_main.action_rewind)->setToolTip(QStringLiteral(""));    //line 321
            ui_main.toolbar_play->widgetForAction(ui_main.action_previous)->setToolTip(QStringLiteral(""));    //line 322
            ui_main.toolbar_play->widgetForAction(ui_main.action_stop)->setToolTip(QStringLiteral(""));    //line 323
            ui_main.toolbar_play->widgetForAction(ui_main.action_play)->setToolTip(QStringLiteral(""));    //line 324
            ui_main.toolbar_play->widgetForAction(ui_main.action_next)->setToolTip(QStringLiteral(""));    //line 325
            ui_main.toolbar_play->widgetForAction(ui_main.action_forward)->setToolTip(QStringLiteral(""));    //line 326
        }    //line 327
    void UIContainer::set_shortcuts(const QKeySequence &key1, const QKeySequence &key2, const QKeySequence &key3, const QKeySequence &key4)    //line 329
        {    //line 330
            ui_main.action_selectmodel->setShortcut(key1);    //line 331
            ui_main.action_initial_state->setShortcut(key2);    //line 332
            ui_main.action_reset_rotation->setShortcut(key3);    //line 333
            ui_main.action_preferences->setShortcut(key4);    //line 334
        }    //line 335
    void UIContainer::add_widgets(QMainWindow *window, QLineEdit *edit, QOpenGLWidget *drawing)    //line 337
        {    //line 338
            edit->setObjectName("edit_moves");    //line 339
            edit->setFrame(false);    //line 340
            ui_main.layout_moves->insertWidget(1, edit);    //line 341
            drawing->setObjectName("drawingarea");    //line 342
            ui_main.verticalLayout->addWidget(drawing);    //line 343
            window->setTabOrder(edit, drawing);    //line 344
            window->setTabOrder(drawing, ui_main.box_sidepane);    //line 345
            drawing->setFocus(Qt::OtherFocusReason);    //line 346
        }    //line 347
    void UIContainer::set_toolbar_visible(bool visible)    //line 350
        { ui_main.toolbar_play->setVisible(visible); }    //line 351
    void UIContainer::set_frame_editbar_visible(bool visible)    //line 353
        { ui_main.frame_editbar->setVisible(visible); }    //line 354
    void UIContainer::set_statusbar_visible(bool visible)    //line 356
        { ui_main.statusbar->setVisible(visible); }    //line 357
    void UIContainer::hide_controls()    //line 359
        {    //line 360
            ui_main.toolbar_play->setVisible(false);    //line 361
            ui_main.toolbar_common->setVisible(false);    //line 362
            ui_main.frame_editbar->setVisible(false);    //line 363
            ui_main.statusbar->setVisible(false);    //line 364
            ui_main.box_sidepane->setVisible(false);    //line 365
        }    //line 366
    void UIContainer::set_visible(bool debug_text_visible, bool editbar_visible, bool statusbar_visible)    //line 369
        {    //line 370
            ui_main.label_debug_text->setVisible(debug_text_visible);    //line 371
            ui_main.action_editbar->setChecked(editbar_visible);    //line 372
            ui_main.action_statusbar->setChecked(statusbar_visible);    //line 373
            ui_main.frame_editbar->setVisible(editbar_visible);    //line 374
            ui_main.statusbar->setVisible(statusbar_visible);    //line 375
        }    //line 376
    void UIContainer::get_splitter_sizes(int &s1, int &s2)    //line 379
        {    //line 380
            QList<int> list(ui_main.splitter->sizes());    //line 381
            s1 = list.at(0);    //line 382
            s2 = list.at(1);    //line 383
        }    //line 384
    void UIContainer::set_splitter_sizes(int s1, int s2)    //line 386
        {    //line 387
            QList<int> list;    //line 388
            list.append(s1);    //line 389
            list.append(s2);    //line 390
            ui_main.splitter->setSizes(list);    //line 391
        }    //line 392
    void UIContainer::splitter_update_minimumsize()    //line 394
        {    //line 395
            int w0, w1;    //line 396
            get_splitter_sizes(w0, w1);    //line 397
            QSize s0 = ui_main.splitter->widget(0)->minimumSizeHint();    //line 398
            QSize s1 = ui_main.splitter->widget(1)->minimumSizeHint();    //line 399
            int w = s0.width() + ui_main.splitter->handleWidth();    //line 400
            int h = std::max(s0.height(), s1.height());    //line 401
            w += w1==0 ? 6 : s1.width();    //line 402
            ui_main.splitter->setMinimumSize(w, h);    //line 403
        }    //line 404
    void UIContainer::set_debug_text(const QString &text)    //line 407
        { ui_main.label_debug_text->setText(text); }    //line 408
    void UIContainer::set_toolbar_state(int a, int b, int c, int d, int e)    //line 411
        {    //line 412
            ui_main.action_rewind->setEnabled(a);    //line 413
            ui_main.action_previous->setEnabled(a);    //line 414
            ui_main.action_play->setEnabled(b);    //line 415
            ui_main.action_next->setEnabled(b);    //line 416
            ui_main.action_forward->setEnabled(b);    //line 417
            ui_main.action_mark_set->setEnabled(c);    //line 418
            ui_main.action_mark_remove->setEnabled(c);    //line 419
            ui_main.action_stop->setVisible(d);    //line 420
            ui_main.action_play->setVisible(!d);    //line 421
            ui_main.action_mark_set->setVisible(e);    //line 422
            ui_main.action_mark_remove->setVisible(!e);    //line 423
            disable_tooltips();    //line 424
        }    //line 425
    void UIContainer::clear_sidepane()    //line 428
        {    //line 429
            QLayoutItem *child;    //line 430
            while ((child = ui_main.layout_sidepane->takeAt(0)) != 0) {    //line 431
                delete child;    //line 432
            }    //line 433
        }    //line 434
    QPushButton *UIContainer::create_sidepane_button(const QString &text)    //line 436
        {    //line 437
            QPushButton *button = new QPushButton(ui_main.box_sidepane);    //line 438
            button->setText(text);    //line 439
            button->setFlat(true);    //line 440
            button->setFocusPolicy(Qt::TabFocus);    //line 441
            ui_main.layout_sidepane->addWidget(button);    //line 442
            return button;    //line 443
        }    //line 444
    QTreeView *UIContainer::create_sidepane_treeview(QStandardItemModel *treestore, int i)    //line 446
        {    //line 447
            QTreeView *treeview = new QTreeView(ui_main.box_sidepane);    //line 448
            treeview->setFrameShape(QFrame::NoFrame);    //line 449
            treeview->setEditTriggers(QAbstractItemView::NoEditTriggers);    //line 450
            treeview->setUniformRowHeights(true);    //line 451
            treeview->setAnimated(true);    //line 452
            treeview->setHeaderHidden(true);    //line 453
            treeview->hide();    //line 454
            treeview->setModel(treestore);    //line 455
            treeview->setRootIndex(treestore->index(i, 0));    //line 456
            ui_main.layout_sidepane->addWidget(treeview);    //line 457
            return treeview;    //line 458
        }    //line 459
    void UIContainer::set_active_plugin_group(int i)    //line 461
        {    //line 462
            QLayoutItem *item_treeview;    //line 463
            if (i == -1)  i = active_plugin_group;    //line 464
            item_treeview = ui_main.layout_sidepane->itemAt(active_plugin_group*2+1);    //line 465
            if (item_treeview != NULL)    //line 466
                item_treeview->widget()->hide();    //line 467
            item_treeview = ui_main.layout_sidepane->itemAt(i*2+1);    //line 468
            if (item_treeview != NULL)    //line 469
                item_treeview->widget()->show();    //line 470
            active_plugin_group = i;    //line 471
        }    //line 472
    void UIContainer::set_active_plugin_group_by_obj(QObject *obj)    //line 474
        {    //line 475
            int i = ui_main.layout_sidepane->indexOf((QWidget*)obj);    //line 476
            this->set_active_plugin_group(i/2);    //line 477
        }    //line 478
    int UIContainer::get_plugin_group_count()    //line 480
        { return ui_main.layout_sidepane->count() / 2; }    //line 481
    QModelIndex UIContainer::root_index(int i)    //line 483
        { return ((QTreeView*)ui_main.layout_sidepane->itemAt(i*2+1)->widget())->rootIndex(); }    //line 484
    void UIContainer::hide_row(int i, bool hide)    //line 486
        {    //line 487
            QWidget *button = ui_main.layout_sidepane->itemAt(i*2)->widget();    //line 488
            if (hide) {    //line 489
                button->hide();    //line 490
                if (i == active_plugin_group)  set_active_plugin_group(0);    //line 491
            } else {    //line 492
                button->show();    //line 493
            }    //line 494
        }    //line 495
    void UIContainer::set_row_hidden(int i, int row, const QModelIndex &index, bool hide)    //line 497
        { ((QTreeView*)ui_main.layout_sidepane->itemAt(i*2+1)->widget())->setRowHidden(row, index, hide); }    //line 498
    int UIContainer::splitter_pos()    //line 500
        { return ui_main.splitter->sizes()[0]; }    //line 501
    void UIContainer::setupUi_pref(QDialog *window, int speed, int speed_min, int speed_max, int mirror_min, int mirror_max, QStandardItemModel *_movekeys)    //line 505
        {    //line 506
            liststore_movekeys = _movekeys;    //line 507
            liststore_movekeys->setObjectName("liststore_movekeys");    //line 508
            liststore_faces = new QStandardItemModel(window);    //line 509
            liststore_faces->setObjectName("liststore_faces");    //line 510

            ui_pref.setupUi(window);    //line 512

            ui_pref.button_animspeed_reset->setIcon(QIcon::fromTheme("edit-clear"));    //line 514
            ui_pref.button_shader_reset->setIcon(QIcon::fromTheme("edit-clear"));    //line 515
            ui_pref.button_antialiasing_reset->setIcon(QIcon::fromTheme("edit-clear"));    //line 516
            ui_pref.button_mirror_faces_reset->setIcon(QIcon::fromTheme("edit-clear"));    //line 517
            ui_pref.button_movekey_add->setIcon(QIcon::fromTheme("list-add"));    //line 518
            ui_pref.button_movekey_remove->setIcon(QIcon::fromTheme("list-remove"));    //line 519
            ui_pref.button_movekey_reset->setIcon(QIcon::fromTheme("document-revert"));    //line 520
            ui_pref.button_color_reset->setIcon(QIcon::fromTheme("edit-clear"));    //line 521
            ui_pref.button_image_reset->setIcon(QIcon::fromTheme("edit-clear"));    //line 522
            ui_pref.button_background_color_reset->setIcon(QIcon::fromTheme("edit-clear"));    //line 523

            ui_pref.buttonBox->button(QDialogButtonBox::Close)->setDefault(true);    //line 525
            ui_pref.label_needs_restarted->setVisible(false);    //line 526
            QPalette pal(ui_pref.label_needs_restarted->palette());    //line 527
            pal.setColor(QPalette::WindowText, QColor(170, 0, 0));    //line 528
            ui_pref.label_needs_restarted->setPalette(pal);    //line 529

            // graphic tab    //line 531
            ui_pref.slider_animspeed->setRange(speed_min, speed_max);    //line 532
            ui_pref.slider_animspeed->setValue(speed);    //line 533
            ui_pref.spinbox_mirror_faces->setRange(mirror_min, mirror_max);    //line 534

            // keys tab    //line 536
            liststore_movekeys->setColumnCount(2);    //line 537
            ui_pref.listview_movekeys->setModel(liststore_movekeys);    //line 538
            ShortcutDelegate *shortcut_delegate = new ShortcutDelegate(ui_pref.listview_movekeys);    //line 539
            ui_pref.listview_movekeys->setItemDelegateForColumn(1, shortcut_delegate);    //line 540

            // theme tab    //line 542
            liststore_faces_width = 30;    //line 543
            iconengine_color = new ColorIconEngine();    //line 544
            ui_pref.button_color->setIcon(QIcon(iconengine_color));    //line 545
            int height = ui_pref.button_color->iconSize().height();    //line 546
            ui_pref.button_color->setIconSize(    //line 547
                    QSize(height * ui_pref.button_color->width() / ui_pref.button_color->height(), height));    //line 548
            ui_pref.button_color->setText(QStringLiteral(""));    //line 549
            iconengine_background_color = new ColorIconEngine();    //line 550
            ui_pref.button_background_color->setIcon(QIcon(iconengine_background_color));    //line 551
            height = ui_pref.button_background_color->iconSize().height();    //line 552
            ui_pref.button_background_color->setIconSize(    //line 553
                    QSize(height * ui_pref.button_background_color->width() / ui_pref.button_background_color->height(), height));    //line 554
            ui_pref.button_background_color->setText(QStringLiteral(""));    //line 555
        }    //line 556
    void UIContainer::preferences_block_signals(bool block)    //line 559
        {    //line 560
            ui_pref.combobox_shader->blockSignals(block);    //line 561
            ui_pref.combobox_samples->blockSignals(block);    //line 562
            liststore_movekeys->blockSignals(block);    //line 563
        }    //line 564
    void UIContainer::combobox_add_shaderitem(const QString &name, const QString &nick)    //line 566
        { ui_pref.combobox_shader->addItem(name, nick); }    //line 567
    void UIContainer::combobox_add_samplesitem(const QString &name, const QString &nick)    //line 569
        { ui_pref.combobox_samples->addItem(name, nick); }    //line 570
    void UIContainer::add_movekey_row()    //line 572
        {    //line 573
            int row = ui_pref.listview_movekeys->currentIndex().row();    //line 574
            liststore_movekeys->insertRow(row);    //line 575
            liststore_movekeys->setItem(row, 0, new QStandardItem(""));    //line 576
            liststore_movekeys->setItem(row, 1, new QStandardItem(""));    //line 577
            QModelIndex index = liststore_movekeys->index(row, 0);    //line 578
            ui_pref.listview_movekeys->setCurrentIndex(index);    //line 579
            ui_pref.listview_movekeys->edit(index);    //line 580
        }    //line 581
    void UIContainer::remove_movekey_row()    //line 583
        {    //line 584
            int row = ui_pref.listview_movekeys->currentIndex().row();    //line 585
            liststore_movekeys->takeRow(row);    //line 586
        }    //line 587
    void UIContainer::add_liststore_faces_row(const QString &name, const QString &key)    //line 589
        {    //line 590
            QStandardItem *item = new QStandardItem(name);    //line 591
            item->setData(QVariant(key));    //line 592
            liststore_faces->appendRow(item);    //line 593
            //XXX: workaround, listview_faces should automatically set to the correct width    //line 594
            QFontMetrics fm(ui_pref.listview_faces->font());    //line 595
            int width = fm.width(name) + 20;  // 20 for the scroll bar    //line 596
            if (liststore_faces_width < width)  liststore_faces_width = width;    //line 597
        }    //line 598
    void UIContainer::finalize_liststore_faces(QWidget *window)    //line 600
        {    //line 601
            ui_pref.listview_faces->setMaximumWidth(liststore_faces_width);    //line 602
            ui_pref.listview_faces->setModel(liststore_faces);    //line 603
            QObject::connect(ui_pref.listview_faces->selectionModel(),    //line 604
                               SIGNAL(currentRowChanged(const QModelIndex &, const QModelIndex &)),    //line 605
                             window,    //line 606
                               SLOT(_on_listview_faces_currentRowChanged(const QModelIndex &)));    //line 607
            ui_pref.listview_faces->setCurrentIndex(liststore_faces->index(0, 0));    //line 608
        }    //line 609
    QString UIContainer::get_liststore_faces_facekey(const QModelIndex &index)    //line 611
        { return liststore_faces->itemFromIndex(index)->data().toString(); }    //line 612
    void UIContainer::set_button_color(const QString &color)    //line 614
        {    //line 615
            iconengine_color->color = color;    //line 616
            ui_pref.button_color->update();    //line 617
        }    //line 618
    QString UIContainer::color_dialog(QWidget *parent, const QString &color)    //line 620
        {    //line 621
            QColorDialog dialog(parent);    //line 622
            dialog.setOption(QColorDialog::DontUseNativeDialog);    //line 623
            dialog.setCurrentColor(QColor(color));    //line 624
            if (dialog.exec() == QDialog::Accepted)    //line 625
                return dialog.currentColor().name();    //line 626
            return QString();    //line 627
        }    //line 628
    void UIContainer::set_imagemode(int imagemode)    //line 630
        {    //line 631
            if (imagemode == 0)    //line 632
                ui_pref.radiobutton_tiled->setChecked(true);    //line 633
            else if (imagemode == 1)    //line 634
                ui_pref.radiobutton_mosaic->setChecked(true);    //line 635
        }    //line 636
    void UIContainer::add_combobox_image_item(const QIcon &icon, const QString &text, const QString &filename)    //line 638
        { ui_pref.combobox_image->addItem(icon, text, filename); }    //line 639
    void UIContainer::set_combobox_image(int index_icon, const QIcon &icon, int index)    //line 641
        {    //line 642
            ui_pref.combobox_image->setItemIcon(index_icon, icon);    //line 643
            if (index >= 0)  ui_pref.combobox_image->setCurrentIndex(index);    //line 644
        }    //line 645
    void UIContainer::set_button_background_color(const QString &color)    //line 648
        {    //line 649
            iconengine_background_color->color = color;    //line 650
            ui_pref.button_background_color->update();    //line 651
        }    //line 652
    void UIContainer::fill_page(QVariant items, const QString &title)    //line 655
        {    //line 656
            ui_main.listwidget->clear();    //line 657
            QList<QObject*> list = items.value<QList<QObject*> >();    //line 658
            for (int i=0; i < list.size(); ++i) {    //line 659
                QObject *obj = list.at(i);    //line 660
                QString text = obj->property("text").value<QString>();    //line 661
                QString filename = obj->property("key").value<QString>();    //line 662
                QListWidgetItem *item = new QListWidgetItem(QIcon(filename), text);    //line 663
                item->setTextAlignment(Qt::AlignHCenter | Qt::AlignBottom);    //line 664
                ui_main.listwidget->addItem(item);    //line 665
            }    //line 666
            label_selectmodel->setText(title);    //line 667
        }    //line 668
    void UIContainer::set_page(int i)    //line 670
        {    //line 671
            ui_main.stackedwidget->setCurrentIndex(i);    //line 672
            ui_main.toolbar_play->setVisible(i==0);    //line 673
            ui_main.toolbar_selectmodel->setVisible(i!=0);    //line 674
        }    //line 675
    void UIContainer::setupUi_help(QDialog *window, const QString &helptext)    //line 678
        {    //line 679
            ui_help.setupUi(window);    //line 680
            ui_help.text_help->setHtml(helptext);    //line 681
        }    //line 682
    void UIContainer::setupUi_about(QDialog *window)    //line 685
        {    //line 686
            ui_about.setupUi(window);    //line 687
            index_tab_about = ui_about.tab_widget->indexOf(ui_about.tab_about);    //line 688
            index_tab_license = ui_about.tab_widget->indexOf(ui_about.tab_license);    //line 689
            // About tab animation    //line 690
            scrollbar = ui_about.text_translators->verticalScrollBar();    //line 691
            animation.setTargetObject(scrollbar);    //line 692
            animation.setPropertyName(QByteArray("value"));    //line 693
            animation.setLoopCount(-1);    //line 694
            ui_about.text_translators->viewport()->installEventFilter(window);    //line 695
        }    //line 696
    void UIContainer::fill_header(const QString &fileName, const QString &appname, const QString &version, const QString &desc)    //line 698
        {    //line 699
            ui_about.label_icon->setPixmap(QPixmap(fileName));    //line 700
            ui_about.label_appname->setText(appname);    //line 701
            ui_about.label_version->setText(version);    //line 702
            ui_about.label_description->setText(desc);    //line 703
        }    //line 704
    void UIContainer::fill_about_tab(const QString &copyr, const QString &website, const QString &translators)    //line 706
        {    //line 707
            ui_about.label_copyright->setText(copyr);    //line 708
            ui_about.label_website->setText(website);    //line 709
            ui_about.text_translators->setHtml(translators);    //line 710
        }    //line 711
    void UIContainer::fill_contribute_tab(const QString &contribute)    //line 713
        { ui_about.label_contribute->setText(contribute); }    //line 714
    void UIContainer::fill_license_tab(const QString &license_short, bool license_notfound, const QString &license_full)    //line 716
        {    //line 717
            ui_about.text_license_short->hide();    //line 718
            ui_about.text_license_full->hide();    //line 719
            ui_about.text_license_short->setHtml(license_short);    //line 720
            if (license_notfound)  ui_about.text_license_full->setLineWrapMode(QTextEdit::WidgetWidth);    //line 721
            ui_about.text_license_full->setHtml(license_full);    //line 722
        }    //line 723
    void UIContainer::tab_widget_currentChanged(int index)    //line 725
        {    //line 726
            if (index == index_tab_about) {    //line 727
                animation.resume();    //line 728
                scrollbar->hide();    //line 729
            } else {    //line 730
                animation.pause();    //line 731
                scrollbar->show();    //line 732
            }    //line 733
            if (index == index_tab_license) {    //line 734
                ui_about.text_license_short->setVisible(true);    //line 735
                ui_about.text_license_full->setVisible(false);    //line 736
            }    //line 737
        }    //line 738
    void UIContainer::update_animation(bool first)    //line 740
        {    //line 741
            int smin, smax;    //line 742
            if (first and animation.state() != QAbstractAnimation::Stopped) return;    //line 743
            smin = scrollbar->minimum();    //line 744
            smax = scrollbar->maximum();    //line 745
            if (smax > smin) {    //line 746
                animation.setDuration((smax-smin) * 40);    //line 747
                animation.setKeyValueAt(0., smin);    //line 748
                animation.setKeyValueAt(0.04, smin);    //line 749
                animation.setKeyValueAt(0.50, smax);    //line 750
                animation.setKeyValueAt(0.54, smax);    //line 751
                animation.setKeyValueAt(1., smin);    //line 752
            }    //line 753
            if (first) {    //line 754
                scrollbar->hide();    //line 755
                animation.start();    //line 756
            }    //line 757
        }    //line 758
    void UIContainer::event_filter_stop_animation(QEvent *event)    //line 760
        {    //line 761
            //assert watched == ui_about.text_translators.viewport()    //line 762
            if ((event->type() == QEvent::MouseButtonPress) || (event->type() == QEvent::Wheel)) {    //line 763
                animation.pause();    //line 764
                scrollbar->show();    //line 765
            }    //line 766
        }    //line 767
    void UIContainer::show_full_license(bool local, const QUrl &url)    //line 769
        {    //line 770
            if (local) {    //line 771
                ui_about.text_license_short->setVisible(false);    //line 772
                ui_about.text_license_full->setVisible(true);    //line 773
            } else    //line 774
                QDesktopServices::openUrl(url);    //line 775
        }    //line 776
QString mk_qstr(const char *pstr, int length)    //line 782
    { return QString::fromUtf8(pstr, length); }    //line 783
