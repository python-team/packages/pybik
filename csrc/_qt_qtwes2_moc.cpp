/****************************************************************************
** Meta object code from reading C++ file '_qt_qtwes2_moc.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "_qt_qtwes2_moc.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file '_qt_qtwes2_moc.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Renderer_t {
    QByteArrayData data[9];
    char stringdata0[111];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Renderer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Renderer_t qt_meta_stringdata_Renderer = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Renderer"
QT_MOC_LITERAL(1, 9, 14), // "picking_result"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 9), // "debug_fps"
QT_MOC_LITERAL(4, 35, 15), // "offscreen_image"
QT_MOC_LITERAL(5, 51, 18), // "on_beforeRendering"
QT_MOC_LITERAL(6, 70, 16), // "on_messageLogged"
QT_MOC_LITERAL(7, 87, 19), // "QOpenGLDebugMessage"
QT_MOC_LITERAL(8, 107, 3) // "msg"

    },
    "Renderer\0picking_result\0\0debug_fps\0"
    "offscreen_image\0on_beforeRendering\0"
    "on_messageLogged\0QOpenGLDebugMessage\0"
    "msg"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Renderer[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       3,    1,   42,    2, 0x06 /* Public */,
       4,    1,   45,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       5,    0,   48,    2, 0x02 /* Public */,
       6,    1,   49,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QImage,    2,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    8,

       0        // eod
};

void Renderer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Renderer *_t = static_cast<Renderer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->picking_result((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->debug_fps((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->offscreen_image((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 3: _t->on_beforeRendering(); break;
        case 4: _t->on_messageLogged((*reinterpret_cast< const QOpenGLDebugMessage(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Renderer::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Renderer::picking_result)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Renderer::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Renderer::debug_fps)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Renderer::*_t)(QImage );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Renderer::offscreen_image)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject Renderer::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Renderer.data,
      qt_meta_data_Renderer,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Renderer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Renderer::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Renderer.stringdata0))
        return static_cast<void*>(const_cast< Renderer*>(this));
    return QObject::qt_metacast(_clname);
}

int Renderer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void Renderer::picking_result(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Renderer::debug_fps(int _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Renderer::offscreen_image(QImage _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
struct qt_meta_stringdata_SectionNameIndexItem_t {
    QByteArrayData data[7];
    char stringdata0[70];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SectionNameIndexItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SectionNameIndexItem_t qt_meta_stringdata_SectionNameIndexItem = {
    {
QT_MOC_LITERAL(0, 0, 20), // "SectionNameIndexItem"
QT_MOC_LITERAL(1, 21, 15), // "section_changed"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 12), // "name_changed"
QT_MOC_LITERAL(4, 51, 7), // "section"
QT_MOC_LITERAL(5, 59, 4), // "name"
QT_MOC_LITERAL(6, 64, 5) // "index"

    },
    "SectionNameIndexItem\0section_changed\0"
    "\0name_changed\0section\0name\0index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SectionNameIndexItem[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       3,   26, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,
       3,    0,   25,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       4, QMetaType::QString, 0x00495003,
       5, QMetaType::QString, 0x00495003,
       6, QMetaType::Int, 0x00095003,

 // properties: notify_signal_id
       0,
       1,
       0,

       0        // eod
};

void SectionNameIndexItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SectionNameIndexItem *_t = static_cast<SectionNameIndexItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->section_changed(); break;
        case 1: _t->name_changed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (SectionNameIndexItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SectionNameIndexItem::section_changed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (SectionNameIndexItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SectionNameIndexItem::name_changed)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        SectionNameIndexItem *_t = static_cast<SectionNameIndexItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->m_section; break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->m_name; break;
        case 2: *reinterpret_cast< int*>(_v) = _t->m_index; break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        SectionNameIndexItem *_t = static_cast<SectionNameIndexItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0:
            if (_t->m_section != *reinterpret_cast< QString*>(_v)) {
                _t->m_section = *reinterpret_cast< QString*>(_v);
                Q_EMIT _t->section_changed();
            }
            break;
        case 1:
            if (_t->m_name != *reinterpret_cast< QString*>(_v)) {
                _t->m_name = *reinterpret_cast< QString*>(_v);
                Q_EMIT _t->name_changed();
            }
            break;
        case 2:
            if (_t->m_index != *reinterpret_cast< int*>(_v)) {
                _t->m_index = *reinterpret_cast< int*>(_v);
            }
            break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject SectionNameIndexItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_SectionNameIndexItem.data,
      qt_meta_data_SectionNameIndexItem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *SectionNameIndexItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SectionNameIndexItem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_SectionNameIndexItem.stringdata0))
        return static_cast<void*>(const_cast< SectionNameIndexItem*>(this));
    return QObject::qt_metacast(_clname);
}

int SectionNameIndexItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void SectionNameIndexItem::section_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void SectionNameIndexItem::name_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
struct qt_meta_stringdata_TextKeyItem_t {
    QByteArrayData data[6];
    char stringdata0[47];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TextKeyItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TextKeyItem_t qt_meta_stringdata_TextKeyItem = {
    {
QT_MOC_LITERAL(0, 0, 11), // "TextKeyItem"
QT_MOC_LITERAL(1, 12, 12), // "text_changed"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 11), // "key_changed"
QT_MOC_LITERAL(4, 38, 4), // "text"
QT_MOC_LITERAL(5, 43, 3) // "key"

    },
    "TextKeyItem\0text_changed\0\0key_changed\0"
    "text\0key"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TextKeyItem[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       2,   26, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,
       3,    0,   25,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       4, QMetaType::QString, 0x00495003,
       5, QMetaType::QString, 0x00495003,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void TextKeyItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TextKeyItem *_t = static_cast<TextKeyItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->text_changed(); break;
        case 1: _t->key_changed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (TextKeyItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TextKeyItem::text_changed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (TextKeyItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&TextKeyItem::key_changed)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        TextKeyItem *_t = static_cast<TextKeyItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->m_text; break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->m_key; break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        TextKeyItem *_t = static_cast<TextKeyItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0:
            if (_t->m_text != *reinterpret_cast< QString*>(_v)) {
                _t->m_text = *reinterpret_cast< QString*>(_v);
                Q_EMIT _t->text_changed();
            }
            break;
        case 1:
            if (_t->m_key != *reinterpret_cast< QString*>(_v)) {
                _t->m_key = *reinterpret_cast< QString*>(_v);
                Q_EMIT _t->key_changed();
            }
            break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject TextKeyItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_TextKeyItem.data,
      qt_meta_data_TextKeyItem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *TextKeyItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TextKeyItem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_TextKeyItem.stringdata0))
        return static_cast<void*>(const_cast< TextKeyItem*>(this));
    return QObject::qt_metacast(_clname);
}

int TextKeyItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void TextKeyItem::text_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void TextKeyItem::key_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
struct qt_meta_stringdata_FacePrefsItem_t {
    QByteArrayData data[10];
    char stringdata0[105];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FacePrefsItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FacePrefsItem_t qt_meta_stringdata_FacePrefsItem = {
    {
QT_MOC_LITERAL(0, 0, 13), // "FacePrefsItem"
QT_MOC_LITERAL(1, 14, 13), // "color_changed"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 14), // "folder_changed"
QT_MOC_LITERAL(4, 44, 13), // "image_changed"
QT_MOC_LITERAL(5, 58, 17), // "imagemode_changed"
QT_MOC_LITERAL(6, 76, 5), // "color"
QT_MOC_LITERAL(7, 82, 6), // "folder"
QT_MOC_LITERAL(8, 89, 5), // "image"
QT_MOC_LITERAL(9, 95, 9) // "imagemode"

    },
    "FacePrefsItem\0color_changed\0\0"
    "folder_changed\0image_changed\0"
    "imagemode_changed\0color\0folder\0image\0"
    "imagemode"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FacePrefsItem[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       4,   38, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    0,   35,    2, 0x06 /* Public */,
       4,    0,   36,    2, 0x06 /* Public */,
       5,    0,   37,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       6, QMetaType::QString, 0x00495003,
       7, QMetaType::QString, 0x00495003,
       8, QMetaType::QString, 0x00495003,
       9, QMetaType::QString, 0x00495003,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,

       0        // eod
};

void FacePrefsItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FacePrefsItem *_t = static_cast<FacePrefsItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->color_changed(); break;
        case 1: _t->folder_changed(); break;
        case 2: _t->image_changed(); break;
        case 3: _t->imagemode_changed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (FacePrefsItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FacePrefsItem::color_changed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (FacePrefsItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FacePrefsItem::folder_changed)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (FacePrefsItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FacePrefsItem::image_changed)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (FacePrefsItem::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&FacePrefsItem::imagemode_changed)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        FacePrefsItem *_t = static_cast<FacePrefsItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->m_color; break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->m_folder; break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->m_image; break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->m_imagemode; break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        FacePrefsItem *_t = static_cast<FacePrefsItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0:
            if (_t->m_color != *reinterpret_cast< QString*>(_v)) {
                _t->m_color = *reinterpret_cast< QString*>(_v);
                Q_EMIT _t->color_changed();
            }
            break;
        case 1:
            if (_t->m_folder != *reinterpret_cast< QString*>(_v)) {
                _t->m_folder = *reinterpret_cast< QString*>(_v);
                Q_EMIT _t->folder_changed();
            }
            break;
        case 2:
            if (_t->m_image != *reinterpret_cast< QString*>(_v)) {
                _t->m_image = *reinterpret_cast< QString*>(_v);
                Q_EMIT _t->image_changed();
            }
            break;
        case 3:
            if (_t->m_imagemode != *reinterpret_cast< QString*>(_v)) {
                _t->m_imagemode = *reinterpret_cast< QString*>(_v);
                Q_EMIT _t->imagemode_changed();
            }
            break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

const QMetaObject FacePrefsItem::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FacePrefsItem.data,
      qt_meta_data_FacePrefsItem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FacePrefsItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FacePrefsItem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FacePrefsItem.stringdata0))
        return static_cast<void*>(const_cast< FacePrefsItem*>(this));
    return QObject::qt_metacast(_clname);
}

int FacePrefsItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void FacePrefsItem::color_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void FacePrefsItem::folder_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void FacePrefsItem::image_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void FacePrefsItem::imagemode_changed()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
struct qt_meta_stringdata_MainView_t {
    QByteArrayData data[43];
    char stringdata0[995];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainView_t qt_meta_stringdata_MainView = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MainView"
QT_MOC_LITERAL(1, 9, 18), // "_on_picking_result"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 5), // "index"
QT_MOC_LITERAL(4, 35, 13), // "_on_debug_fps"
QT_MOC_LITERAL(5, 49, 3), // "fps"
QT_MOC_LITERAL(6, 53, 19), // "_on_offscreen_image"
QT_MOC_LITERAL(7, 73, 5), // "image"
QT_MOC_LITERAL(8, 79, 22), // "on_beforeSynchronizing"
QT_MOC_LITERAL(9, 102, 24), // "on_sceneGraphInvalidated"
QT_MOC_LITERAL(10, 127, 29), // "on_action_challenge_triggered"
QT_MOC_LITERAL(11, 157, 30), // "on_action_new_solved_triggered"
QT_MOC_LITERAL(12, 188, 31), // "on_action_preferences_triggered"
QT_MOC_LITERAL(13, 220, 34), // "on_action_reset_rotation_trig..."
QT_MOC_LITERAL(14, 255, 26), // "on_action_rewind_triggered"
QT_MOC_LITERAL(15, 282, 28), // "on_action_previous_triggered"
QT_MOC_LITERAL(16, 311, 24), // "on_action_stop_triggered"
QT_MOC_LITERAL(17, 336, 24), // "on_action_play_triggered"
QT_MOC_LITERAL(18, 361, 24), // "on_action_next_triggered"
QT_MOC_LITERAL(19, 386, 27), // "on_action_forward_triggered"
QT_MOC_LITERAL(20, 414, 28), // "on_action_mark_set_triggered"
QT_MOC_LITERAL(21, 443, 31), // "on_action_mark_remove_triggered"
QT_MOC_LITERAL(22, 475, 33), // "on_action_initial_state_trigg..."
QT_MOC_LITERAL(23, 509, 29), // "on_action_edit_cube_triggered"
QT_MOC_LITERAL(24, 539, 31), // "on_action_selectmodel_triggered"
QT_MOC_LITERAL(25, 571, 36), // "on_action_selectmodel_back_tr..."
QT_MOC_LITERAL(26, 608, 27), // "on_listwidget_itemActivated"
QT_MOC_LITERAL(27, 636, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(28, 653, 4), // "item"
QT_MOC_LITERAL(29, 658, 25), // "on_listwidget_itemClicked"
QT_MOC_LITERAL(30, 684, 24), // "on_action_quit_triggered"
QT_MOC_LITERAL(31, 709, 25), // "on_action_editbar_toggled"
QT_MOC_LITERAL(32, 735, 7), // "checked"
QT_MOC_LITERAL(33, 743, 27), // "on_action_statusbar_toggled"
QT_MOC_LITERAL(34, 771, 24), // "on_action_help_triggered"
QT_MOC_LITERAL(35, 796, 24), // "on_action_info_triggered"
QT_MOC_LITERAL(36, 821, 25), // "on_splitter_splitterMoved"
QT_MOC_LITERAL(37, 847, 3), // "pos"
QT_MOC_LITERAL(38, 851, 28), // "on_button_edit_clear_clicked"
QT_MOC_LITERAL(39, 880, 27), // "on_button_edit_exec_clicked"
QT_MOC_LITERAL(40, 908, 35), // "on_action_jump_to_editbar_tri..."
QT_MOC_LITERAL(41, 944, 27), // "_on_button_sidepane_clicked"
QT_MOC_LITERAL(42, 972, 22) // "_on_treeview_activated"

    },
    "MainView\0_on_picking_result\0\0index\0"
    "_on_debug_fps\0fps\0_on_offscreen_image\0"
    "image\0on_beforeSynchronizing\0"
    "on_sceneGraphInvalidated\0"
    "on_action_challenge_triggered\0"
    "on_action_new_solved_triggered\0"
    "on_action_preferences_triggered\0"
    "on_action_reset_rotation_triggered\0"
    "on_action_rewind_triggered\0"
    "on_action_previous_triggered\0"
    "on_action_stop_triggered\0"
    "on_action_play_triggered\0"
    "on_action_next_triggered\0"
    "on_action_forward_triggered\0"
    "on_action_mark_set_triggered\0"
    "on_action_mark_remove_triggered\0"
    "on_action_initial_state_triggered\0"
    "on_action_edit_cube_triggered\0"
    "on_action_selectmodel_triggered\0"
    "on_action_selectmodel_back_triggered\0"
    "on_listwidget_itemActivated\0"
    "QListWidgetItem*\0item\0on_listwidget_itemClicked\0"
    "on_action_quit_triggered\0"
    "on_action_editbar_toggled\0checked\0"
    "on_action_statusbar_toggled\0"
    "on_action_help_triggered\0"
    "on_action_info_triggered\0"
    "on_splitter_splitterMoved\0pos\0"
    "on_button_edit_clear_clicked\0"
    "on_button_edit_exec_clicked\0"
    "on_action_jump_to_editbar_triggered\0"
    "_on_button_sidepane_clicked\0"
    "_on_treeview_activated"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainView[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      34,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,  184,    2, 0x02 /* Public */,
       4,    1,  187,    2, 0x02 /* Public */,
       6,    1,  190,    2, 0x02 /* Public */,
       8,    0,  193,    2, 0x02 /* Public */,
       9,    0,  194,    2, 0x02 /* Public */,
      10,    0,  195,    2, 0x02 /* Public */,
      11,    0,  196,    2, 0x02 /* Public */,
      12,    0,  197,    2, 0x02 /* Public */,
      13,    0,  198,    2, 0x02 /* Public */,
      14,    0,  199,    2, 0x02 /* Public */,
      15,    0,  200,    2, 0x02 /* Public */,
      16,    0,  201,    2, 0x02 /* Public */,
      17,    0,  202,    2, 0x02 /* Public */,
      18,    0,  203,    2, 0x02 /* Public */,
      19,    0,  204,    2, 0x02 /* Public */,
      20,    0,  205,    2, 0x02 /* Public */,
      21,    0,  206,    2, 0x02 /* Public */,
      22,    0,  207,    2, 0x02 /* Public */,
      23,    0,  208,    2, 0x02 /* Public */,
      24,    0,  209,    2, 0x02 /* Public */,
      25,    0,  210,    2, 0x02 /* Public */,
      26,    1,  211,    2, 0x02 /* Public */,
      29,    1,  214,    2, 0x02 /* Public */,
      30,    0,  217,    2, 0x02 /* Public */,
      31,    1,  218,    2, 0x02 /* Public */,
      33,    1,  221,    2, 0x02 /* Public */,
      34,    0,  224,    2, 0x02 /* Public */,
      35,    0,  225,    2, 0x02 /* Public */,
      36,    2,  226,    2, 0x02 /* Public */,
      38,    0,  231,    2, 0x02 /* Public */,
      39,    0,  232,    2, 0x02 /* Public */,
      40,    0,  233,    2, 0x02 /* Public */,
      41,    0,  234,    2, 0x02 /* Public */,
      42,    1,  235,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::QImage,    7,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 27,   28,
    QMetaType::Void, 0x80000000 | 27,   28,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   32,
    QMetaType::Void, QMetaType::Bool,   32,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   37,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    3,

       0        // eod
};

void MainView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainView *_t = static_cast<MainView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->_on_picking_result((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->_on_debug_fps((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->_on_offscreen_image((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 3: _t->on_beforeSynchronizing(); break;
        case 4: _t->on_sceneGraphInvalidated(); break;
        case 5: _t->on_action_challenge_triggered(); break;
        case 6: _t->on_action_new_solved_triggered(); break;
        case 7: _t->on_action_preferences_triggered(); break;
        case 8: _t->on_action_reset_rotation_triggered(); break;
        case 9: _t->on_action_rewind_triggered(); break;
        case 10: _t->on_action_previous_triggered(); break;
        case 11: _t->on_action_stop_triggered(); break;
        case 12: _t->on_action_play_triggered(); break;
        case 13: _t->on_action_next_triggered(); break;
        case 14: _t->on_action_forward_triggered(); break;
        case 15: _t->on_action_mark_set_triggered(); break;
        case 16: _t->on_action_mark_remove_triggered(); break;
        case 17: _t->on_action_initial_state_triggered(); break;
        case 18: _t->on_action_edit_cube_triggered(); break;
        case 19: _t->on_action_selectmodel_triggered(); break;
        case 20: _t->on_action_selectmodel_back_triggered(); break;
        case 21: _t->on_listwidget_itemActivated((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 22: _t->on_listwidget_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 23: _t->on_action_quit_triggered(); break;
        case 24: _t->on_action_editbar_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 25: _t->on_action_statusbar_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: _t->on_action_help_triggered(); break;
        case 27: _t->on_action_info_triggered(); break;
        case 28: _t->on_splitter_splitterMoved((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 29: _t->on_button_edit_clear_clicked(); break;
        case 30: _t->on_button_edit_exec_clicked(); break;
        case 31: _t->on_action_jump_to_editbar_triggered(); break;
        case 32: _t->_on_button_sidepane_clicked(); break;
        case 33: _t->_on_treeview_activated((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject MainView::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainView.data,
      qt_meta_data_MainView,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainView::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainView.stringdata0))
        return static_cast<void*>(const_cast< MainView*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 34)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 34;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 34)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 34;
    }
    return _id;
}
struct qt_meta_stringdata_MoveEdit_t {
    QByteArrayData data[3];
    char stringdata0[27];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MoveEdit_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MoveEdit_t qt_meta_stringdata_MoveEdit = {
    {
QT_MOC_LITERAL(0, 0, 8), // "MoveEdit"
QT_MOC_LITERAL(1, 9, 16), // "on_returnpressed"
QT_MOC_LITERAL(2, 26, 0) // ""

    },
    "MoveEdit\0on_returnpressed\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MoveEdit[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void,

       0        // eod
};

void MoveEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MoveEdit *_t = static_cast<MoveEdit *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_returnpressed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject MoveEdit::staticMetaObject = {
    { &QLineEdit::staticMetaObject, qt_meta_stringdata_MoveEdit.data,
      qt_meta_data_MoveEdit,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MoveEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MoveEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MoveEdit.stringdata0))
        return static_cast<void*>(const_cast< MoveEdit*>(this));
    return QLineEdit::qt_metacast(_clname);
}

int MoveEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLineEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_DrawingArea_t {
    QByteArrayData data[1];
    char stringdata0[12];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DrawingArea_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DrawingArea_t qt_meta_stringdata_DrawingArea = {
    {
QT_MOC_LITERAL(0, 0, 11) // "DrawingArea"

    },
    "DrawingArea"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DrawingArea[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void DrawingArea::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject DrawingArea::staticMetaObject = {
    { &QOpenGLWidget::staticMetaObject, qt_meta_stringdata_DrawingArea.data,
      qt_meta_data_DrawingArea,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *DrawingArea::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DrawingArea::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_DrawingArea.stringdata0))
        return static_cast<void*>(const_cast< DrawingArea*>(this));
    return QOpenGLWidget::qt_metacast(_clname);
}

int DrawingArea::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QOpenGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_PreferencesDialog_t {
    QByteArrayData data[33];
    char stringdata0[909];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PreferencesDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PreferencesDialog_t qt_meta_stringdata_PreferencesDialog = {
    {
QT_MOC_LITERAL(0, 0, 17), // "PreferencesDialog"
QT_MOC_LITERAL(1, 18, 32), // "on_slider_animspeed_valueChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 5), // "value"
QT_MOC_LITERAL(4, 58, 33), // "on_button_animspeed_reset_cli..."
QT_MOC_LITERAL(5, 92, 38), // "on_combobox_shader_currentInd..."
QT_MOC_LITERAL(6, 131, 30), // "on_button_shader_reset_clicked"
QT_MOC_LITERAL(7, 162, 39), // "on_combobox_samples_currentIn..."
QT_MOC_LITERAL(8, 202, 36), // "on_button_antialiasing_reset_..."
QT_MOC_LITERAL(9, 239, 32), // "on_checkbox_mirror_faces_toggled"
QT_MOC_LITERAL(10, 272, 7), // "checked"
QT_MOC_LITERAL(11, 280, 36), // "on_spinbox_mirror_faces_value..."
QT_MOC_LITERAL(12, 317, 36), // "on_button_mirror_faces_reset_..."
QT_MOC_LITERAL(13, 354, 32), // "on_button_mousemode_quad_toggled"
QT_MOC_LITERAL(14, 387, 31), // "on_button_mousemode_ext_toggled"
QT_MOC_LITERAL(15, 419, 35), // "on_button_mousemode_gesture_t..."
QT_MOC_LITERAL(16, 455, 33), // "on_liststore_movekeys_itemCha..."
QT_MOC_LITERAL(17, 489, 14), // "QStandardItem*"
QT_MOC_LITERAL(18, 504, 11), // "unused_item"
QT_MOC_LITERAL(19, 516, 29), // "on_button_movekey_add_clicked"
QT_MOC_LITERAL(20, 546, 32), // "on_button_movekey_remove_clicked"
QT_MOC_LITERAL(21, 579, 31), // "on_button_movekey_reset_clicked"
QT_MOC_LITERAL(22, 611, 36), // "_on_listview_faces_currentRow..."
QT_MOC_LITERAL(23, 648, 7), // "current"
QT_MOC_LITERAL(24, 656, 23), // "on_button_color_clicked"
QT_MOC_LITERAL(25, 680, 29), // "on_button_color_reset_clicked"
QT_MOC_LITERAL(26, 710, 27), // "on_combobox_image_activated"
QT_MOC_LITERAL(27, 738, 5), // "index"
QT_MOC_LITERAL(28, 744, 29), // "on_button_image_reset_clicked"
QT_MOC_LITERAL(29, 774, 28), // "on_radiobutton_tiled_toggled"
QT_MOC_LITERAL(30, 803, 29), // "on_radiobutton_mosaic_toggled"
QT_MOC_LITERAL(31, 833, 34), // "on_button_background_color_cl..."
QT_MOC_LITERAL(32, 868, 40) // "on_button_background_color_re..."

    },
    "PreferencesDialog\0on_slider_animspeed_valueChanged\0"
    "\0value\0on_button_animspeed_reset_clicked\0"
    "on_combobox_shader_currentIndexChanged\0"
    "on_button_shader_reset_clicked\0"
    "on_combobox_samples_currentIndexChanged\0"
    "on_button_antialiasing_reset_clicked\0"
    "on_checkbox_mirror_faces_toggled\0"
    "checked\0on_spinbox_mirror_faces_valueChanged\0"
    "on_button_mirror_faces_reset_clicked\0"
    "on_button_mousemode_quad_toggled\0"
    "on_button_mousemode_ext_toggled\0"
    "on_button_mousemode_gesture_toggled\0"
    "on_liststore_movekeys_itemChanged\0"
    "QStandardItem*\0unused_item\0"
    "on_button_movekey_add_clicked\0"
    "on_button_movekey_remove_clicked\0"
    "on_button_movekey_reset_clicked\0"
    "_on_listview_faces_currentRowChanged\0"
    "current\0on_button_color_clicked\0"
    "on_button_color_reset_clicked\0"
    "on_combobox_image_activated\0index\0"
    "on_button_image_reset_clicked\0"
    "on_radiobutton_tiled_toggled\0"
    "on_radiobutton_mosaic_toggled\0"
    "on_button_background_color_clicked\0"
    "on_button_background_color_reset_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PreferencesDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,  139,    2, 0x02 /* Public */,
       4,    0,  142,    2, 0x02 /* Public */,
       5,    1,  143,    2, 0x02 /* Public */,
       6,    0,  146,    2, 0x02 /* Public */,
       7,    1,  147,    2, 0x02 /* Public */,
       8,    0,  150,    2, 0x02 /* Public */,
       9,    1,  151,    2, 0x02 /* Public */,
      11,    1,  154,    2, 0x02 /* Public */,
      12,    0,  157,    2, 0x02 /* Public */,
      13,    1,  158,    2, 0x02 /* Public */,
      14,    1,  161,    2, 0x02 /* Public */,
      15,    1,  164,    2, 0x02 /* Public */,
      16,    1,  167,    2, 0x02 /* Public */,
      19,    0,  170,    2, 0x02 /* Public */,
      20,    0,  171,    2, 0x02 /* Public */,
      21,    0,  172,    2, 0x02 /* Public */,
      22,    1,  173,    2, 0x02 /* Public */,
      24,    0,  176,    2, 0x02 /* Public */,
      25,    0,  177,    2, 0x02 /* Public */,
      26,    1,  178,    2, 0x02 /* Public */,
      28,    0,  181,    2, 0x02 /* Public */,
      29,    1,  182,    2, 0x02 /* Public */,
      30,    1,  185,    2, 0x02 /* Public */,
      31,    0,  188,    2, 0x02 /* Public */,
      32,    0,  189,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Double,    3,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,   23,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   27,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void PreferencesDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PreferencesDialog *_t = static_cast<PreferencesDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_slider_animspeed_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->on_button_animspeed_reset_clicked(); break;
        case 2: _t->on_combobox_shader_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_button_shader_reset_clicked(); break;
        case 4: _t->on_combobox_samples_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->on_button_antialiasing_reset_clicked(); break;
        case 6: _t->on_checkbox_mirror_faces_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->on_spinbox_mirror_faces_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 8: _t->on_button_mirror_faces_reset_clicked(); break;
        case 9: _t->on_button_mousemode_quad_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->on_button_mousemode_ext_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->on_button_mousemode_gesture_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->on_liststore_movekeys_itemChanged((*reinterpret_cast< QStandardItem*(*)>(_a[1]))); break;
        case 13: _t->on_button_movekey_add_clicked(); break;
        case 14: _t->on_button_movekey_remove_clicked(); break;
        case 15: _t->on_button_movekey_reset_clicked(); break;
        case 16: _t->_on_listview_faces_currentRowChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 17: _t->on_button_color_clicked(); break;
        case 18: _t->on_button_color_reset_clicked(); break;
        case 19: _t->on_combobox_image_activated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->on_button_image_reset_clicked(); break;
        case 21: _t->on_radiobutton_tiled_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: _t->on_radiobutton_mosaic_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: _t->on_button_background_color_clicked(); break;
        case 24: _t->on_button_background_color_reset_clicked(); break;
        default: ;
        }
    }
}

const QMetaObject PreferencesDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_PreferencesDialog.data,
      qt_meta_data_PreferencesDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *PreferencesDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PreferencesDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_PreferencesDialog.stringdata0))
        return static_cast<void*>(const_cast< PreferencesDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int PreferencesDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}
struct qt_meta_stringdata_HelpDialog_t {
    QByteArrayData data[1];
    char stringdata0[11];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_HelpDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_HelpDialog_t qt_meta_stringdata_HelpDialog = {
    {
QT_MOC_LITERAL(0, 0, 10) // "HelpDialog"

    },
    "HelpDialog"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_HelpDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void HelpDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObject HelpDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_HelpDialog.data,
      qt_meta_data_HelpDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *HelpDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *HelpDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_HelpDialog.stringdata0))
        return static_cast<void*>(const_cast< HelpDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int HelpDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
struct qt_meta_stringdata_AboutDialog_t {
    QByteArrayData data[8];
    char stringdata0[127];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AboutDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AboutDialog_t qt_meta_stringdata_AboutDialog = {
    {
QT_MOC_LITERAL(0, 0, 11), // "AboutDialog"
QT_MOC_LITERAL(1, 12, 33), // "on_text_translators_anchorCli..."
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 4), // "link"
QT_MOC_LITERAL(4, 52, 28), // "on_tab_widget_currentChanged"
QT_MOC_LITERAL(5, 81, 5), // "index"
QT_MOC_LITERAL(6, 87, 35), // "on_text_license_short_anchorC..."
QT_MOC_LITERAL(7, 123, 3) // "url"

    },
    "AboutDialog\0on_text_translators_anchorClicked\0"
    "\0link\0on_tab_widget_currentChanged\0"
    "index\0on_text_license_short_anchorClicked\0"
    "url"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AboutDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x02 /* Public */,
       4,    1,   32,    2, 0x02 /* Public */,
       6,    1,   35,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, QMetaType::QUrl,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::QUrl,    7,

       0        // eod
};

void AboutDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        AboutDialog *_t = static_cast<AboutDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_text_translators_anchorClicked((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 1: _t->on_tab_widget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->on_text_license_short_anchorClicked((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject AboutDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_AboutDialog.data,
      qt_meta_data_AboutDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *AboutDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AboutDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_AboutDialog.stringdata0))
        return static_cast<void*>(const_cast< AboutDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int AboutDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
